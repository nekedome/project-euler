"""
:mod:`tests.unit.test_sequence` -- Unit Tests
=============================================

.. module:: tests.unit.test_sequence
   :synopsis: Unit tests for the lib.sequence module.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

import pytest
from typing import Set

from lib.sequence import Factorials, Fibonaccis
from lib.sequence import Figurates, Triangulars, Pentagonals, Hexagonals
from lib.sequence import Pandigitals, Divisors, DivisorsRange, Primes
from lib.sequence import ContinuedFraction, SqrtExpansion
from lib.util import load_dataset


# Some prime number constants used to test lib.sequence.Primes
SMALL_LIMIT = 10
PRIMES_UP_TO_SMALL_LIMIT = [2, 3, 5, 7]
FIRST_SMALL_LIMIT_PRIMES = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]


@pytest.mark.parametrize("n,n_factorial", [(0, 1), (1, 1), (2, 2), (3, 6), (4, 24), (5, 120), (12, 479001600),
                                           (15, 1307674368000), (20, 2432902008176640000)])
def test_factorials_correctness_getitem(n: int, n_factorial: int):
    """ Test the correctness of indexing :class:`lib.sequence.Factorials` instances using known answer tests

    :param n: the input value
    :param n_factorial: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Factorials` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Factorials` produces the wrong value
    """

    seq = Factorials()
    computed_answer = seq[n]
    assert isinstance(computed_answer, Factorials.__getitem__.__annotations__["return"]), "wrong type"
    assert computed_answer == n_factorial, "wrong value"


def test_factorials_correctness_batch():
    """ Test the correctness of :class:`lib.sequence.Factorials` using known data """
    factorial_numbers = load_dataset("general", "factorial", data_type=int)
    for n, seq_n in enumerate(Factorials()):
        if n >= len(factorial_numbers):
            break
        assert isinstance(seq_n, Factorials.__next__.__annotations__["return"]), "wrong type"
        assert seq_n == factorial_numbers[n], "wrong value"


def test_factorials_len():
    """ Test that ``len`` of :class:`lib.sequence.Factorials` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Factorials()
        len(seq)


@pytest.mark.parametrize("x,is_factorial", [(-10, False), (0, False), (1, True), (6, True), (120, True), (121, False)])
def test_factorials_correctness_in(x: int, is_factorial: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Factorials` instances using known answer tests

    :param x: the input value
    :param is_factorial: whether :math:`x = N!` :math:`\\exists N \\in \\mathbb{N}` (i.e. :math:`x` is factorial)
    :raises AssertionError: if :class:`lib.sequence.Factorials` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Factorials` produces the wrong value
    """

    computed_answer = x in Factorials()
    assert isinstance(computed_answer, Factorials.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == is_factorial, "wrong value"


def test_factorials_getitem_bad_item_type_str():
    """ Test that :class:`lib.sequence.Factorials` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``str``)
    """
    with pytest.raises(TypeError):
        seq = Factorials()
        _ = seq["bam"]


def test_factorials_getitem_bad_item_type_float():
    """ Test that :class:`lib.sequence.Factorials` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``float``)
    """
    with pytest.raises(TypeError):
        seq = Factorials()
        _ = seq[12.3]


def test_factorials_getitem_bad_item_negative():
    """ Test that :class:`lib.sequence.Factorials` raises a ``ValueError`` for an index :math:`\\mbox{item} \\lt 0` """
    with pytest.raises(ValueError):
        seq = Factorials()
        _ = seq[-2]


def test_factorials_in_bad_item_type_str():
    """ Test that :class:`lib.sequence.Factorials` raises a ``TypeError`` for a non-``int`` value for `x` (``str``) """
    with pytest.raises(TypeError):
        seq = Factorials()
        _ = "abc" in seq


@pytest.mark.parametrize("n,nth_fibonacci", [(0, 0), (1, 1), (2, 1), (3, 2), (4, 3), (5, 5), (6, 8), (7, 13), (8, 21),
                                             (9, 34), (10, 55), (11, 89), (12, 144), (13, 233), (14, 377), (15, 610)])
def test_fibonaccis_correctness_getitem(n: int, nth_fibonacci: int):
    """ Test the correctness of :class:`lib.sequence.Fibonaccis` using known answer tests

    :param n: the input value
    :param nth_fibonacci: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Fibonaccis` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Fibonaccis` produces the wrong value
    """

    seq = Fibonaccis()
    computed_answer = seq[n]
    assert isinstance(computed_answer, Fibonaccis.__getitem__.__annotations__["return"]), "wrong type"
    assert computed_answer == nth_fibonacci, "wrong value"


def test_fibonaccis_correctness_batch():
    """ Test the correctness of :class:`lib.sequence.Fibonaccis` using known data """
    fibonacci_numbers = load_dataset("general", "fibonacci", data_type=int)
    for n, seq_n in enumerate(Fibonaccis()):
        if n == 0:
            continue
        elif n >= len(fibonacci_numbers) - 1:
            break
        else:
            assert seq_n == fibonacci_numbers[n - 1]


def test_fibonaccis_len():
    """ Test that ``len`` of :class:`lib.sequence.Fibonaccis` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Fibonaccis()
        len(seq)


@pytest.mark.parametrize("x,is_fibonacci", [(-10, False), (0, True), (1, True), (6, False), (21, True), (22, False)])
def test_fibonaccis_correctness_in(x: int, is_fibonacci: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Fibonaccis` instances using known answer tests

    :param x: the input value
    :param is_fibonacci: whether :math:`x` is a Fibonacci number or not
    :raises AssertionError: if :class:`lib.sequence.Fibonaccis` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Fibonaccis` produces the wrong value
    """

    computed_answer = x in Fibonaccis()
    assert isinstance(computed_answer, Fibonaccis.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == is_fibonacci, "wrong value"


def test_fibonaccis_getitem_bad_item_type_str():
    """ Test that :class:`lib.sequence.Fibonaccis` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``str``)
    """
    with pytest.raises(TypeError):
        seq = Fibonaccis()
        _ = seq["bam"]


def test_fibonaccis_getitem_bad_item_type_float():
    """ Test that :class:`lib.sequence.Fibonaccis` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``float``)
    """
    with pytest.raises(TypeError):
        seq = Fibonaccis()
        _ = seq[12.3]


def test_fibonaccis_getitem_bad_item_negative():
    """ Test that :class:`lib.sequence.Fibonaccis` raises a ``ValueError`` for an index :math:`\\mbox{item} \\lt 0` """
    with pytest.raises(ValueError):
        seq = Fibonaccis()
        _ = seq[-2]


def test_fibonaccis_in_bad_item_type_str():
    """ Test that :class:`lib.sequence.Fibonaccis` raises a ``TypeError`` for a non-``int`` value for `x` (``str``) """
    with pytest.raises(TypeError):
        seq = Fibonaccis()
        _ = "abc" in seq


def test_figurates_next():
    """ Test that iteration over :class:`lib.sequence.Figurates` instances raises a ``NotImplementedError`` """
    with pytest.raises(NotImplementedError):
        seq = Figurates()
        next(iter(seq))


def test_figurates_len():
    """ Test that ``len`` of :class:`lib.sequence.Figurates` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Figurates()
        len(seq)


def test_figurates_in():
    """ Test that membership tests for :class:`lib.sequence.Figurates` instances raises a ``NotImplementedError`` """
    with pytest.raises(NotImplementedError):
        seq = Figurates()
        _ = 0 in seq


@pytest.mark.parametrize("n,nth_triangular", [(1, 1), (2, 3), (3, 6), (4, 10), (5, 15), (6, 21), (7, 28), (8, 36),
                                              (9, 45), (10, 55), (11, 66), (12, 78), (13, 91), (14, 105), (15, 120)])
def test_triangulars_correctness_getitem(n: int, nth_triangular: int):
    """ Test the correctness of :class:`lib.sequence.Triangulars` using known answer tests

    :param n: the input value
    :param nth_triangular: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong value
    """

    seq = Triangulars()
    assert isinstance(seq[n], Triangulars.__getitem__.__annotations__["return"]), "wrong type"
    assert seq[n] == nth_triangular, "wrong value"


def test_triangulars_correctness_iter():
    """ Test the correctness of iteration over :class:`lib.sequence.Triangulars` instances using known answer tests

    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong value
    """

    expected_answer = [1, 3, 6, 10, 15, 21, 28, 36, 45, 55]
    seq = Triangulars()
    for n, nth_triangular in enumerate(seq):
        if n >= len(expected_answer):
            break
        assert isinstance(nth_triangular, Triangulars.__next__.__annotations__["return"]), "wrong type"
        assert nth_triangular == expected_answer[n], "wrong value"


def test_triangulars_len():
    """ Test that ``len`` of :class:`lib.sequence.Triangulars` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Triangulars()
        len(seq)


def test_triangulars_getitem_bad_item_type_str():
    """ Test that :class:`lib.sequence.Triangulars` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``str``)
    """
    with pytest.raises(TypeError):
        seq = Triangulars()
        _ = seq["bam"]


def test_triangulars_getitem_bad_item_type_float():
    """ Test that :class:`lib.sequence.Triangulars` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``float``)
    """
    with pytest.raises(TypeError):
        seq = Triangulars()
        _ = seq[12.3]


@pytest.mark.parametrize("x,expected_answer", [(1, True), (2, False), (3, True), (50, False)])
def test_triangulars_correctness_in(x: int, expected_answer: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Triangulars` instances using known answer
    tests

    :param x: the input value
    :param expected_answer: whether :math:`x` is a triangular number or not
    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Triangulars` produces the wrong value
    """

    seq = Triangulars()
    computed_answer = x in seq
    assert isinstance(computed_answer, Triangulars.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong value"


def test_triangulars_in_bad_item_type_str():
    """ Test that membership tests for :class:`lib.sequence.Triangulars` instances raise a ``TypeError`` for a
    non-``int`` value (``str``)
    """
    with pytest.raises(TypeError):
        seq = Triangulars()
        _ = "abc" in seq


def test_triangulars_in_bad_item_type_float():
    """ Test that membership tests for :class:`lib.sequence.Triangulars` instances raise a ``TypeError`` for a
    non-``int`` value (``float``)
    """
    with pytest.raises(TypeError):
        seq = Triangulars()
        _ = 1.23 in seq


@pytest.mark.parametrize("n,nth_pentagonal", [(1, 1), (2, 5), (3, 12), (4, 22), (5, 35), (6, 51), (7, 70), (8, 92),
                                              (9, 117), (10, 145), (11, 176), (12, 210), (13, 247), (14, 287)])
def test_pentagonals_correctness_getitem(n: int, nth_pentagonal: int):
    """ Test the correctness of :class:`lib.sequence.Pentagonals` using known answer tests

    :param n: the input value
    :param nth_pentagonal: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong value
    """

    seq = Pentagonals()
    assert isinstance(seq[n], Pentagonals.__getitem__.__annotations__["return"]), "wrong type"
    assert seq[n] == nth_pentagonal, "wrong value"


def test_pentagonals_correctness_iter():
    """ Test the correctness of iteration over :class:`lib.sequence.Pentagonals` instances using known answer tests

    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong value
    """

    expected_answer = [1, 5, 12, 22, 35, 51, 70, 92, 117, 145]
    seq = Pentagonals()
    for n, nth_pentagonal in enumerate(seq):
        if n >= len(expected_answer):
            break
        assert isinstance(nth_pentagonal, Pentagonals.__next__.__annotations__["return"]), "wrong type"
        assert nth_pentagonal == expected_answer[n], "wrong value"


def test_pentagonals_len():
    """ Test that ``len`` of :class:`lib.sequence.Pentagonals` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Pentagonals()
        len(seq)


def test_pentagonals_getitem_bad_item_type_str():
    """ Test that :class:`lib.sequence.Pentagonals` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``str``)
    """
    with pytest.raises(TypeError):
        seq = Pentagonals()
        _ = seq["bam"]


def test_pentagonals_getitem_bad_item_type_float():
    """ Test that :class:`lib.sequence.Pentagonals` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``float``)
    """
    with pytest.raises(TypeError):
        seq = Pentagonals()
        _ = seq[12.3]


@pytest.mark.parametrize("x,expected_answer", [(1, True), (2, False), (35, True), (50, False)])
def test_pentagonals_correctness_in(x: int, expected_answer: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Pentagonals` instances using known answer
    tests

    :param x: the input value
    :param expected_answer: whether :math:`x` is a pentagonal number or not
    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Pentagonals` produces the wrong value
    """

    seq = Pentagonals()
    computed_answer = x in seq
    assert isinstance(computed_answer, Pentagonals.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong value"


def test_pentagonals_in_bad_item_type_str():
    """ Test that membership tests for :class:`lib.sequence.Pentagonals` instances raise a ``TypeError`` for a
    non-``int`` value (``str``)
    """
    with pytest.raises(TypeError):
        seq = Pentagonals()
        _ = "abc" in seq


def test_pentagonals_in_bad_item_type_float():
    """ Test that membership tests for :class:`lib.sequence.Pentagonals` instances raise a ``TypeError`` for a
    non-``int`` value (``float``)
    """
    with pytest.raises(TypeError):
        seq = Pentagonals()
        _ = 1.23 in seq


@pytest.mark.parametrize("n,nth_hexagonal", [(1, 1), (2, 6), (3, 15), (4, 28), (5, 45), (6, 66), (7, 91), (8, 120),
                                             (9, 153), (10, 190), (11, 231), (12, 276), (13, 325), (14, 378)])
def test_hexagonals_correctness_getitem(n: int, nth_hexagonal: int):
    """ Test the correctness of :class:`lib.sequence.Hexagonals` using known answer tests

    :param n: the input value
    :param nth_hexagonal: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong value
    """

    seq = Hexagonals()
    assert isinstance(seq[n], Hexagonals.__getitem__.__annotations__["return"]), "wrong type"
    assert seq[n] == nth_hexagonal, "wrong value"


def test_hexagonals_correctness_iter():
    """ Test the correctness of iteration over :class:`lib.sequence.Hexagonals` instances using known answer tests

    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong value
    """

    expected_answer = [1, 6, 15, 28, 45, 66, 91, 120, 153, 190]
    seq = Hexagonals()
    for n, nth_hexagonal in enumerate(seq):
        if n >= len(expected_answer):
            break
        assert isinstance(nth_hexagonal, Hexagonals.__next__.__annotations__["return"]), "wrong type"
        assert nth_hexagonal == expected_answer[n], "wrong value"


def test_hexagonals_len():
    """ Test that ``len`` of :class:`lib.sequence.Hexagonals` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Hexagonals()
        len(seq)


def test_hexagonals_getitem_bad_item_type_str():
    """ Test that :class:`lib.sequence.Hexagonals` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``str``)
    """
    with pytest.raises(TypeError):
        seq = Hexagonals()
        _ = seq["bam"]


def test_hexagonals_getitem_bad_item_type_float():
    """ Test that :class:`lib.sequence.Hexagonals` raises a ``TypeError`` for a non-``int`` value for an index `item`
    (``float``)
    """
    with pytest.raises(TypeError):
        seq = Hexagonals()
        _ = seq[12.3]


@pytest.mark.parametrize("x,expected_answer", [(1, True), (2, False), (45, True), (50, False)])
def test_hexagonals_correctness_in(x: int, expected_answer: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Hexagonals` instances using known answer tests

    :param x: the input value
    :param expected_answer: whether :math:`x` is a hexagonal number or not
    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Hexagonals` produces the wrong value
    """

    seq = Hexagonals()
    computed_answer = x in seq
    assert isinstance(computed_answer, Hexagonals.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong value"


def test_hexagonals_in_bad_item_type_str():
    """ Test that membership tests for :class:`lib.sequence.Hexagonals` instances raise a ``TypeError`` for a
    non-``int`` value (``str``)
    """
    with pytest.raises(TypeError):
        seq = Hexagonals()
        _ = "abc" in seq


def test_hexagonals_in_bad_item_type_float():
    """ Test that membership tests for :class:`lib.sequence.Hexagonals` instances raise a ``TypeError`` for a
    non-``int`` value (``float``)
    """
    with pytest.raises(TypeError):
        seq = Hexagonals()
        _ = 1.23 in seq


@pytest.mark.parametrize("n,n_pandigitals", [(1, {1}), (2, {12, 21}), (3, {123, 132, 213, 231, 312, 321})])
def test_pandigitals_correctness(n: int, n_pandigitals: Set[int]):
    """ Test the correctness of :class:`lib.sequence.Pandigitals` using known answer tests

    :param n: the input value
    :param n_pandigitals: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Pandigitals` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Pandigitals` produces the wrong value
    """

    seq = set(Pandigitals(n))
    for elt in seq:
        assert isinstance(elt, Pandigitals.__next__.__annotations__["return"]), "wrong type"
    assert set(seq) == n_pandigitals, "wrong value"


@pytest.mark.parametrize("n,n_pandigitals", [(1, {1}), (2, {12, 21}), (3, {123, 132, 213, 231, 312, 321})])
def test_pandigitals_correctness_len(n: int, n_pandigitals: Set[int]):
    """ Test the correctness of length calculations of :class:`lib.sequence.Pandigitals` instances using known answer
    tests

    :param n: the input value
    :param n_pandigitals: the expected number of :math:`n`-pandigitals
    :raises AssertionError: if :class:`lib.sequence.Pandigitals` produces the wrong value
    """

    seq = Pandigitals(n)
    assert len(seq) == len(n_pandigitals), "wrong value"


def test_pandigitals_bad_n_type_str():
    """ Test that :class:`lib.sequence.Pandigitals` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        Pandigitals("abc")


def test_pandigitals_bad_n_type_float():
    """ Test that :class:`lib.sequence.Pandigitals` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
    """
    with pytest.raises(TypeError):
        Pandigitals(1.23)


def test_pandigitals_bad_n_negative():
    """ Test that :class:`lib.sequence.Pandigitals` raises a ``ValueError`` for :math:`\\mbox{n} \\lt 0` """
    with pytest.raises(ValueError):
        Pandigitals(-14)


@pytest.mark.parametrize("n,proper,expected_answer", [(10, True, {1, 2, 5}), (10, False, {1, 2, 5, 10}), (5, True, {1}),
                                                      (12, False, {1, 2, 3, 4, 6, 12}), (7, False, {1, 7}),
                                                      (1, False, {1}), (1, True, set())])
def test_divisors_correctness(n: int, proper: bool, expected_answer: Set[int]):
    """ Test the correctness of :class:`lib.sequence.Divisors` using known answer tests

    :param n: the input value
    :param proper: whether to only consider proper divisors or not
    :param expected_answer: the expected answer
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong value
    """

    seq = set(Divisors(n, proper))
    for elt in seq:
        assert isinstance(elt, Divisors.__next__.__annotations__["return"]), "wrong type"
    assert len(seq) == len(expected_answer), "wrong number of divisors"
    assert set(seq) == expected_answer, "wrong divisors"


def test_divisors_bad_n_type_str():
    """ Test that :class:`lib.sequence.Divisors` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        Divisors("abc", proper=False)


def test_divisors_bad_n_type_float():
    """ Test that :class:`lib.sequence.Divisors` raises a ``TypeError`` for a non-``int`` value for `n` (``float``) """
    with pytest.raises(TypeError):
        Divisors(1.23, proper=False)


def test_divisors_bad_n_zero():
    """ Test that :class:`lib.sequence.Divisors` raises a ``ValueError`` for :math:`\\mbox{n} = 0` """
    with pytest.raises(ValueError):
        Divisors(0, proper=False)


def test_divisors_bad_proper_type_str():
    """ Test that :class:`lib.sequence.Divisors` raises a ``TypeError`` for a non-``bool`` value for `proper` (``str``)
    """
    with pytest.raises(TypeError):
        Divisors(10, proper="abc")


def test_divisors_bad_proper_type_float():
    """ Test that :class:`lib.sequence.Divisors` raises a ``TypeError`` for a non-``bool`` value for `proper`
    (``float``)
    """
    with pytest.raises(TypeError):
        Divisors(10, proper=12.3)


@pytest.mark.parametrize("n,n_divisors", [(10, 4), (5, 2), (12, 6), (7, 2), (1, 1), (22, 4)])
def test_divisors_correctness_len(n: int, n_divisors: int):
    """ Test the correctness of length calculations of :class:`lib.sequence.Divisors` instances using known answer tests

    :param n: the input value
    :param n_divisors: the expected number of divisors (all, not proper)
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong value for `proper` = ``False``
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong value for `proper` = ``True``
    """

    assert len(Divisors(n, proper=False)) == n_divisors, "wrong number of divisors"
    assert len(Divisors(n, proper=True)) == (n_divisors - 1), "wrong number of proper divisors"


@pytest.mark.parametrize("n,proper,d,expected_answer", [(10, True, 5, True), (10, True, 10, False),
                                                        (4, True, 5, False), (50, False, 10, True)])
def test_divisors_correctness_in(n: int, proper: bool, d: int, expected_answer: bool):
    """ Test the correctness of membership tests for :class:`lib.sequence.Divisors` instances using known answer tests

    :param n: the input value
    :param proper: whether to only consider proper divisors or not
    :param d: the input divisor
    :param expected_answer: whether :math:`d` is a [proper] divisor of :math:`n`
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong value for `proper` = ``False``
    :raises AssertionError: if :class:`lib.sequence.Divisors` produces the wrong value for `proper` = ``True``
    """

    seq = Divisors(n, proper)
    computed_answer = d in seq
    assert isinstance(computed_answer, Divisors.__contains__.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong value"


def test_divisors_in_bad_item_type_str():
    """ Test that :class:`lib.sequence.Divisors` raises a ``TypeError`` for a non-``int`` value for `item` (``str``) """
    with pytest.raises(TypeError):
        _ = "abc" in Divisors(10, proper=False)


@pytest.mark.parametrize("proper", [True, False])
def test_divisorsrange_correctness(proper: bool):
    """ Test the correctness of :class:`lib.sequence.DivisorsRange` using known answer tests

    The divisors yielded by this sieve are tested to ensure that they do divide the given integer :math:`n`, up to some
    moderate bound on :math:`n`. Additionally, it is checked that :math:`1` is included as a proper divisor of
    :math:`n` and that :math:`n` is **not**.

    :param proper: whether to only consider proper divisors or not
    :raises AssertionError: if :class:`lib.sequence.DivisorsRange` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.DivisorsRange` produces the wrong value
    """

    for n, divisors in enumerate(DivisorsRange(100, proper)):
        for divisor in divisors:
            # assert isinstance(divisor, DivisorsRange.__next__.__annotations__["return"]), "wrong type"
            assert (n + 1) % divisor == 0, "non-divisor included by DivisorsRange"
        assert 1 in divisors, "wrong answer: 1 divides everything"
        if proper:
            assert (n + 1) not in divisors or (n + 1) <= 1, "wrong answer: n is not a proper divisor of itself"
        else:
            assert (n + 1) in divisors, "wrong answer: n is a divisor of itself"


def test_divisorsrange_bad_upper_bound_type_str():
    """ Test that :class:`lib.sequence.DivisorsRange` raises a ``TypeError`` for a non-``int`` value for `upper_bound`
    (``str``)
    """
    with pytest.raises(TypeError):
        DivisorsRange("abc", proper=False)


def test_divisorsrange_bad_upper_bound_type_float():
    """ Test that :class:`lib.sequence.DivisorsRange` raises a ``TypeError`` for a non-``int`` value for `upper_bound`
    (``float``)
    """
    with pytest.raises(TypeError):
        DivisorsRange(1.23, proper=False)


def test_divisorsrange_bad_upper_bound_zero():
    """ Test that :class:`lib.sequence.DivisorsRange` raises a ``ValueError`` for :math:`upper\\_bound = 0` """
    with pytest.raises(ValueError):
        DivisorsRange(0, proper=False)


def test_divisorsrange_bad_proper_type_str():
    """ Test that :class:`lib.sequence.DivisorsRange` raises a ``TypeError`` for a non-``bool`` value for `proper`
    (``str``)
    """
    with pytest.raises(TypeError):
        DivisorsRange(10, proper="abc")


def test_divisorsrange_bad_proper_type_float():
    """ Test that :class:`lib.sequence.DivisorsRange` raises a ``TypeError`` for a non-``bool`` value for `proper`
    (``float``)
    """
    with pytest.raises(TypeError):
        DivisorsRange(10, proper=12.3)


def test_primes_correctness_upper_bound():
    """ Test the correctness of :class:`lib.sequence.Primes` (with `upper_bound`) using known answer tests """
    seq = Primes(upper_bound=10)
    assert list(seq) == PRIMES_UP_TO_SMALL_LIMIT, "wrong value"


def test_primes_correctness_upper_bound_iter():
    """ Test the correctness of iteration over :class:`lib.sequence.Primes` (with `upper_bound`) using known answer
    tests

    :raises AssertionError: if :class:`lib.sequence.Primes` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Primes` produces the wrong value
    """

    seq = Primes(upper_bound=SMALL_LIMIT)
    for prime in seq:
        assert isinstance(prime, Primes.__next__.__annotations__["return"]), "wrong type"
        assert prime in PRIMES_UP_TO_SMALL_LIMIT, "wrong value"


def test_primes_bad_upper_bound_str():
    """ Test that :func:`lib.sequence.Primes` raises a ``TypeError`` for a non-``int`` value for `upper_bound` (``str``)
    """
    with pytest.raises(TypeError):
        Primes(upper_bound="100")


def test_primes_bad_upper_bound_float():
    """ Test that :func:`lib.sequence.Primes` raises a ``TypeError`` for a non-``int`` value for `upper_bound`
    (``float``)
    """
    with pytest.raises(TypeError):
        Primes(upper_bound=1.23)


def test_primes_bad_upper_bound_zero():
    """ Test that :func:`lib.sequence.Primes` raises a ``ValueError`` for :math:`\\mbox{upper_bound} = 0` """
    with pytest.raises(ValueError):
        Primes(upper_bound=0)


def test_primes_correctness_n_primes():
    """ Test the correctness of :class:`lib.sequence.Primes` (with `n_primes`) using known answer tests """
    seq = list(Primes(n_primes=SMALL_LIMIT))
    assert seq == FIRST_SMALL_LIMIT_PRIMES, "wrong value"
    assert len(seq) == SMALL_LIMIT, "wrong length"


def test_primes_correctness_n_primes_edge_case():
    """ Test the edge case where the estimate for :math:`p_n` based on `n_primes` doesn't hold (:math:`n \\le 3`) """
    seq = list(Primes(n_primes=3))
    assert seq == [2, 3, 5], "wrong value"
    assert len(seq) == 3, "wrong length"


def test_primes_correctness_n_primes_iter():
    """ Test the correctness of iteration over :class:`lib.sequence.Primes` (with `n_primes`) using known answer tests

    :raises AssertionError: if :class:`lib.sequence.Primes` produces the wrong type
    :raises AssertionError: if :class:`lib.sequence.Primes` produces the wrong value
    """

    seq = list(Primes(n_primes=SMALL_LIMIT))
    for prime in seq:
        assert isinstance(prime, Primes.__next__.__annotations__["return"]), "wrong type"
        assert prime in FIRST_SMALL_LIMIT_PRIMES, "wrong value"
    assert len(seq) == SMALL_LIMIT, "wrong length"


def test_primes_bad_n_primes_str():
    """ Test that :func:`lib.sequence.Primes` raises a ``TypeError`` for a non-``int`` value for `n_primes` (``str``)
    """
    with pytest.raises(TypeError):
        Primes(n_primes="100")


def test_primes_bad_n_primes_float():
    """ Test that :func:`lib.sequence.Primes` raises a ``TypeError`` for a non-``int`` value for `n_primes` (``float``)
    """
    with pytest.raises(TypeError):
        Primes(n_primes=1.23)


def test_primes_bad_n_primes_negative():
    """ Test that :func:`lib.sequence.Primes` raises a ``ValueError`` for :math:`\\mbox{n_primes} \\lt 0` """
    with pytest.raises(ValueError):
        Primes(n_primes=-1)


def test_primes_correctness_batch():
    """ Test the correctness of :class:`lib.sequence.Primes` using known data """
    primes = load_dataset("general", "primes", data_type=int)
    largest_prime = max(primes)
    seq = Primes(upper_bound=largest_prime)
    for n, prime_n in enumerate(seq):
        assert prime_n == primes[n]


def test_primes_len():
    """ Test that ``len`` of :class:`lib.sequence.Primes` instances raises a ``TypeError`` """
    with pytest.raises(TypeError):
        seq = Primes(upper_bound=SMALL_LIMIT)
        len(seq)


def test_continued_fraction():
    """ FIXME: unit tests still to be defined """
    pass


def test_sqrt_expansion():
    """ FIXME: unit tests still to be defined """
    pass
