"""
:mod:`tests.unit.test_digital` -- Unit Tests
============================================

.. module:: tests.unit.test_digital
   :synopsis: Unit tests for the lib.digital module.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from typing import List, Union
import pytest


class TestDigitsOf(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.digits_of` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.digits_of` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.digits_of` function
        """
        from lib.digital import digits_of
        return digits_of

    @pytest.mark.parametrize("n,base,expected_answer", [(10, 10, [1, 0]), (1579, 10, [1, 5, 7, 9]), (7, 2, [1, 1, 1]),
                                                        (10, 2, [1, 0, 1, 0]), (0, 9, [0])])
    def test_digits_of_correctness(self, n: int, base: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.digits_of` using known answer tests

        :param n: the input integer
        :param base: the base to build digits in
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.digits_of` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.digits_of` returns the wrong value
        """

        digits_of = self.get_import()
        computed_answer = digits_of(n, base=base)
        # assert isinstance(computed_answer, digits_of.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_digits_of_bad_n_type_str(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
        digits_of = self.get_import()
        with pytest.raises(TypeError):
            digits_of("123")

    def test_digits_of_bad_n_type_float(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
        """
        digits_of = self.get_import()
        with pytest.raises(TypeError):
            digits_of(123.0)

    def test_digits_of_bad_n_negative(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``ValueError`` for a negative value for `n` """
        digits_of = self.get_import()
        with pytest.raises(ValueError):
            digits_of(-123)

    def test_digits_of_bad_base_type_str(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``TypeError`` for a non-``int`` value for `base` (``str``)
        """
        digits_of = self.get_import()
        with pytest.raises(TypeError):
            digits_of(123, base="2")

    def test_digits_of_bad_base_type_float(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        digits_of = self.get_import()
        with pytest.raises(TypeError):
            digits_of(123, base=2.0)

    def test_digits_of_bad_base_negative(self):
        """ Test that :func:`lib.digital.digits_of` raises a ``ValueError`` for a negative value for `base` """
        digits_of = self.get_import()
        with pytest.raises(ValueError):
            digits_of(123, base=-2)


class TestDigitsToNum(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.digits_to_num` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.digits_to_sum` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.digits_to_num` function
        """
        from lib.digital import digits_to_num
        return digits_to_num

    @pytest.mark.parametrize("digits,base,expected_answer", [([1, 2, 3], 10, 123), ([3, 2, 1], 10, 321), ([7], 10, 7),
                                                             ([1, 0, 1, 0], 2, 10), ([1, 1, 1], 2, 7)])
    def test_digits_to_num_correctness(self, digits: List[int], base: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.digits_to_num` using known answer tests

        :param digits: the input digits
        :param base: the base to build digits in
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.digits_to_num` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.digits_to_num` returns the wrong value
        """

        digits_to_num = self.get_import()
        computed_answer = digits_to_num(digits, base=base)
        assert isinstance(computed_answer, digits_to_num.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_digits_to_num_bad_digits_type_str(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a non-``list`` value for `digits`
        (``str``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num("123")

    def test_digits_to_num_bad_digits_type_float(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a non-``list`` value for `digits`
        (``float``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num(123.0)

    def test_digits_to_num_bad_digits_type_list_of_str(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a list of non-``int`` values for
        `digits` (list of ``str``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num(["123"])

    def test_digits_to_num_bad_digits_type_list_of_float(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a list of non-``int`` values for
        `digits` (list of ``float``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num([123.0])

    def test_digits_to_num_bad_digits_negative_element(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``ValueError`` for a negative value in `digits`
        """
        digits_to_num = self.get_import()
        with pytest.raises(ValueError):
            digits_to_num([-1, 2, 3])

    def test_digits_to_num_bad_base_type_str(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a non-``int`` value for `base`
        (``str``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num([1, 2, 3], base="2")

    def test_digits_to_num_bad_base_type_float(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        digits_to_num = self.get_import()
        with pytest.raises(TypeError):
            digits_to_num([1, 2, 3], base=2.0)

    def test_digits_to_num_bad_base_negative(self):
        """ Test that :func:`lib.digital.digits_to_num` raises a ``ValueError`` for a negative value for `base` """
        digits_to_num = self.get_import()
        with pytest.raises(ValueError):
            digits_to_num(123, base=-2)


class TestDigitSum(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.digit_sum` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.digit_sum` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.digit_sum` function
        """
        from lib.digital import digit_sum
        return digit_sum

    @pytest.mark.parametrize("value,expected_answer", [(0, 0), (9, 9), (11, 2), (123, 6), (654321, 21)])
    def test_digit_sum_correctness(self, value: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.digit_sum` using known answer tests

        :param value: the input value
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.digit_sum` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.digit_sum` returns the wrong value
        """

        digit_sum = self.get_import()
        computed_answer = digit_sum(value)
        assert isinstance(computed_answer, digit_sum.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_digit_sum_bad_n_type_str(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
        digit_sum = self.get_import()
        with pytest.raises(TypeError):
            digit_sum("123")

    def test_digit_sum_bad_n_type_float(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
        """
        digit_sum = self.get_import()
        with pytest.raises(TypeError):
            digit_sum(123.0)

    def test_digit_sum_bad_n_negative(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``ValueError`` for a negative value for `n` """
        digit_sum = self.get_import()
        with pytest.raises(ValueError):
            digit_sum(-123)

    def test_digit_sum_bad_base_type_str(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``TypeError`` for a non-``int`` value for `base` (``str``)
        """
        digit_sum = self.get_import()
        with pytest.raises(TypeError):
            digit_sum(123, base="2")

    def test_digit_sum_bad_base_type_float(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        digit_sum = self.get_import()
        with pytest.raises(TypeError):
            digit_sum(123, base=2.0)

    def test_digit_sum_bad_base_negative(self):
        """ Test that :func:`lib.digital.digit_sum` raises a ``ValueError`` for a negative value for `base` """
        digit_sum = self.get_import()
        with pytest.raises(ValueError):
            digit_sum(123, base=-2)


class TestNumDigits(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.num_digits` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.num_digits` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.num_digits` function
        """
        from lib.digital import num_digits
        return num_digits

    @pytest.mark.parametrize("value,expected_answer",
                             [(0, 1), (9, 1), (10, 2), (55, 2), (99, 2), (100, 3), (999, 3), (1000, 4)])
    def test_num_digits_correctness_kat(self, value: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.num_digits` using known answer tests

        :param value: the input value
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.num_digits` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.num_digits` returns the wrong value
        """

        num_digits = self.get_import()
        computed_answer = num_digits(value)
        assert isinstance(computed_answer, num_digits.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_num_digits_correctness_random(self):
        """ Test the correctness of :func:`lib.digital.num_digits` using random inputs with predictable answers

        That is, generate a random length (`n`) and a random integer of that length (`x`). Then, check that
        :func:`lib.digital.num_digits` evaluated at `x` equals  `n`.

        :raises AssertionError: if :func:`lib.digital.num_digits` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.num_digits` returns the wrong value
        """

        from random import randint
        num_digits = self.get_import()
        for i in range(1000):  # a moderate but arbitrary number of random tests
            digit_len = randint(1, 50)  # a moderate but arbitrary length (n)
            random_integer = randint(10 ** (digit_len - 1), 10 ** digit_len - 1)  # the random number (x)
            computed_answer = num_digits(random_integer)
            assert isinstance(computed_answer, num_digits.__annotations__["return"]), "wrong type"
            assert computed_answer == digit_len, "wrong answer"

    def test_num_digits_bad_n_type_str(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
        """
        num_digits = self.get_import()
        with pytest.raises(TypeError):
            num_digits("123")

    def test_num_digits_bad_n_type_float(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
        """
        num_digits = self.get_import()
        with pytest.raises(TypeError):
            num_digits(123.0)

    def test_num_digits_bad_n_negative(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``ValueError`` for a negative value for `n` """
        num_digits = self.get_import()
        with pytest.raises(ValueError):
            num_digits(-123)

    def test_num_digits_bad_base_type_str(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``TypeError`` for a non-``int`` value for `base` (``str``)
        """
        num_digits = self.get_import()
        with pytest.raises(TypeError):
            num_digits(123, base="2")

    def test_num_digits_bad_base_type_float(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        num_digits = self.get_import()
        with pytest.raises(TypeError):
            num_digits(123, base=2.0)

    def test_num_digits_bad_base_negative(self):
        """ Test that :func:`lib.digital.num_digits` raises a ``ValueError`` for a negative value for `base` """
        num_digits = self.get_import()
        with pytest.raises(ValueError):
            num_digits(123, base=-2)


class TestIsPandigital(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.is_pandigital` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.is_pandigital` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.is_pandigital` function
        """
        from lib.digital import is_pandigital
        return is_pandigital

    @pytest.mark.parametrize("value,d,expected_answer",
                             [(123, 3, True), (200, 3, False), (123, 4, False), ([1, 25, 43], 5, True),
                              ([1, 2, 2], 3, False), ([1, 2, 3, 4], 3, False)])
    def test_is_pandigital_correctness(self, value: Union[int, List[int]], d: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.is_pandigital` using known answer tests

        :param value: the input value
        :param d: parameter in `d`-pandigital
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.is_pandigital` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.is_pandigital` returns the wrong value
        """

        is_pandigital = self.get_import()
        computed_answer = is_pandigital(value, d)
        assert isinstance(computed_answer, is_pandigital.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_is_pandigital_bad_int_n_type_str(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital("123", 3)

    def test_is_pandigital_bad_int_n_type_float(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `n`
        (``float``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital(123.0, 3)

    def test_is_pandigital_bad_list_n_type_str(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital([1, 2, "3"], 3)

    def test_is_pandigital_bad_list_n_type_float(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `n`
        (``float``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital([1, 2.3], 3)

    def test_is_pandigital_bad_int_n_negative(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``ValueError`` for a negative value for `n` """
        is_pandigital = self.get_import()
        with pytest.raises(ValueError):
            is_pandigital(-123, 3)

    def test_is_pandigital_bad_list_n_negative(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``ValueError`` for a negative value for `n` """
        is_pandigital = self.get_import()
        with pytest.raises(ValueError):
            is_pandigital([-1, 2, 3], 3)

    def test_is_pandigital_bad_d_type_str(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `d` (``str``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital(123, "3")

    def test_is_pandigital_bad_d_type_float(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `d`
        (``float``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital(123, 3.0)

    def test_is_pandigital_bad_d_negative(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``ValueError`` for a negative value for `d` """
        is_pandigital = self.get_import()
        with pytest.raises(ValueError):
            is_pandigital(123, -3)

    def test_is_pandigital_bad_lower_type_str(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `lower`
        (``str``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital([1, 2, 3], 3, lower="10")

    def test_is_pandigital_bad_lower_type_float(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `lower`
        (``float``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital([1, 2, 3], 3, lower=10.0)

    def test_is_pandigital_bad_lower_negative(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``ValueError`` for a negative value for `lower` """
        is_pandigital = self.get_import()
        with pytest.raises(ValueError):
            is_pandigital(123, 3, lower=-10)

    def test_is_pandigital_bad_base_type_str(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `base`
        (``str``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital(123, 3, base="10")

    def test_is_pandigital_bad_base_type_float(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        is_pandigital = self.get_import()
        with pytest.raises(TypeError):
            is_pandigital(123, 3, base=2.0)

    def test_is_pandigital_bad_base_negative(self):
        """ Test that :func:`lib.digital.is_pandigital` raises a ``ValueError`` for a negative value for `base` """
        is_pandigital = self.get_import()
        with pytest.raises(ValueError):
            is_pandigital(123, 3, base=-10)


class TestIsPalindrome(object):
    """ A collection of positive and negative unit tests for the :func:`lib.digital.is_palindrome` function """

    @classmethod
    def get_import(cls):
        """ Get and return the :func:`lib.digital.is_palindrome` function

        .. note:: this isolates the import so any error/exception is raised by individual unit tests.

        :return: the :func:`lib.digital.is_palindrome` function
        """
        from lib.digital import is_palindrome
        return is_palindrome

    @pytest.mark.parametrize("value,base,expected_answer", [(121, 10, True), (123, 10, False), (5, 2, True)])
    def test_is_palindrome_correctness(self, value: int, base: int, expected_answer: int):
        """ Test the correctness of :func:`lib.digital.is_palindrome` using known answer tests

        :param value: the input value
        :param base: consider `value` in the given `base`
        :param expected_answer: the expected answer
        :raises AssertionError: if :func:`lib.digital.is_palindrome` returns the wrong type
        :raises AssertionError: if :func:`lib.digital.is_palindrome` returns the wrong value
        """

        is_palindrome = self.get_import()
        computed_answer = is_palindrome(value, base=base)
        assert isinstance(computed_answer, is_palindrome.__annotations__["return"]), "wrong type"
        assert computed_answer == expected_answer, "wrong answer"

    def test_is_palindrome_bad_n_type_str(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
        """
        is_palindrome = self.get_import()
        with pytest.raises(TypeError):
            is_palindrome("123")

    def test_is_palindrome_bad_n_type_float(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``TypeError`` for a non-``int`` value for `n`
        (``float``)
        """
        is_palindrome = self.get_import()
        with pytest.raises(TypeError):
            is_palindrome(123.0)

    def test_is_palindrome_bad_n_negative(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``ValueError`` for a negative value for `n` """
        is_palindrome = self.get_import()
        with pytest.raises(ValueError):
            is_palindrome(-123)

    def test_is_palindrome_bad_base_type_str(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``TypeError`` for a non-``int`` value for `base`
        (``str``)
        """
        is_palindrome = self.get_import()
        with pytest.raises(TypeError):
            is_palindrome(123, base="2")

    def test_is_palindrome_bad_base_type_float(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``TypeError`` for a non-``int`` value for `base`
        (``float``)
        """
        is_palindrome = self.get_import()
        with pytest.raises(TypeError):
            is_palindrome(123, base=2.0)

    def test_is_palindrome_bad_base_negative(self):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``ValueError`` for a negative value for `base` """
        is_palindrome = self.get_import()
        with pytest.raises(ValueError):
            is_palindrome(123, base=-2)

    @pytest.mark.parametrize("base", [1, 3, 4, 5, 6, 7, 9, 11, 12, 13, 14, 15, 200])
    def test_is_palindrome_unsupported_base(self, base: int):
        """ Test that :func:`lib.digital.is_palindrome` raises a ``ValueError`` for unsupported values of `base`

        :param base: consider `value` in the given `base`
        """

        is_palindrome = self.get_import()
        with pytest.raises(ValueError):
            is_palindrome(111, base=base)
