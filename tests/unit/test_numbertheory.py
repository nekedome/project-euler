"""
:mod:`tests.unit.test_numbertheory` -- Unit Tests
=================================================

.. module:: tests.unit.test_numbertheory
   :synopsis: Unit tests for the lib.numbertheory package.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from collections import Counter
from itertools import takewhile
import pytest
from random import choice, randint
from typing import Optional, Union

from lib.util import load_dataset
from lib.numbertheory import is_even, is_odd, is_square
from lib.numbertheory import divisor_count, divisor_sum, divisor_sum_aliquot
from lib.numbertheory import is_probably_prime, prime_sieve, divisors_sieve
from lib.numbertheory.primality import miller_rabin
from lib.numbertheory.sieve import eratosthenes, segmented_eratosthenes
from lib.numbertheory import factor
from lib.numbertheory.factorisation import perfect_power_exponent


@pytest.mark.parametrize("value,expected_answer", [(0, True), (1, False), (200, True), (-123, False), (-24, True)])
def test_is_even_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.is_even` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.is_even` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.is_even` returns the wrong value
    """

    computed_answer = is_even(value)
    assert isinstance(computed_answer, is_even.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_is_even_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.is_even` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        is_even("123")


def test_is_even_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.is_even` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
    """
    with pytest.raises(TypeError):
        is_even(123.0)


@pytest.mark.parametrize("value,expected_answer", [(0, False), (1, True), (200, False), (-123, True), (-24, False)])
def test_is_odd_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.is_odd` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.is_odd` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.is_odd` returns the wrong value
    """

    computed_answer = is_odd(value)
    assert isinstance(computed_answer, is_odd.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_is_odd_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.is_odd` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        is_odd("123")


def test_is_odd_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.is_odd` raises a ``TypeError`` for a non-``int`` value for `n` (``float``) """
    with pytest.raises(TypeError):
        is_odd(123.0)


@pytest.mark.parametrize("value,expected_answer", [(0, True), (1, True), (122, False), (16, True), (10000, True)])
def test_is_square_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.is_square` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.is_square` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.is_square` returns the wrong value
    """

    computed_answer = is_square(value)
    assert isinstance(computed_answer, is_square.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_is_square_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.is_square` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
    """
    with pytest.raises(TypeError):
        is_square("123")


def test_is_square_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.is_square` raises a ``TypeError`` for a non-``int`` value for `n` (``float``)
    """
    with pytest.raises(TypeError):
        is_square(123.0)


@pytest.mark.parametrize("value,expected_answer", [(1, 1), (17, 2), (10, 4), (30, 8), (275, 6), (10000, 25)])
def test_divisor_count_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.divisor_count` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.divisor_count` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisor_count` returns the wrong value
    """

    computed_answer = divisor_count(value)
    assert isinstance(computed_answer, divisor_count.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_divisor_count_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.divisor_count` raises a ``TypeError`` for a non-``int`` value for `n`
    (``str``)
    """
    with pytest.raises(TypeError):
        divisor_count("123")


def test_divisor_count_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.divisor_count` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        divisor_count(123.0)


def test_divisor_count_bad_n_zero():
    """ Test that :func:`lib.numbertheory.divisor_count` raises a ``ValueError`` for a non-positive value for `n` """
    with pytest.raises(ValueError):
        divisor_count(0)


@pytest.mark.parametrize("value,expected_answer", [(1, 1), (17, 18), (10, 18), (30, 72), (275, 372), (10000, 24211)])
def test_divisor_sum_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.divisor_sum` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.divisor_sum` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisor_sum` returns the wrong value
    """

    computed_answer = divisor_sum(value)
    assert isinstance(computed_answer, divisor_sum.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_divisor_sum_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.divisor_sum` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
    """
    with pytest.raises(TypeError):
        divisor_sum("123")


def test_divisor_sum_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.divisor_sum` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        divisor_sum(123.0)


def test_divisor_sum_bad_n_zero():
    """ Test that :func:`lib.numbertheory.divisor_sum` raises a ``ValueError`` for a non-positive value for `n` """
    with pytest.raises(ValueError):
        divisor_sum(0)


@pytest.mark.parametrize("value,expected_answer", [(1, 0), (17, 1), (10, 8), (30, 42), (275, 97), (10000, 14211)])
def test_divisor_sum_aliquot_correctness(value: int, expected_answer: bool):
    """ Test the correctness of :func:`lib.numbertheory.divisor_sum_aliquot` using known answer tests

    :param value: the input value
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.divisor_sum_aliquot` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisor_sum_aliquot` returns the wrong value
    """

    computed_answer = divisor_sum_aliquot(value)
    assert isinstance(computed_answer, divisor_sum_aliquot.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_divisor_sum_aliquot_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.divisor_sum_aliquot` raises a ``TypeError`` for a non-``int`` value for `n`
    (``str``)
    """
    with pytest.raises(TypeError):
        divisor_sum_aliquot("123")


def test_divisor_sum_aliquot_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.divisor_sum_aliquot` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        divisor_sum_aliquot(123.0)


def test_divisor_sum_aliquot_bad_n_zero():
    """ Test that :func:`lib.numbertheory.divisor_sum_aliquot` raises a ``ValueError`` for a non-positive value for `n`
    """
    with pytest.raises(ValueError):
        divisor_sum_aliquot(0)


def test_is_probably_prime_correctness():
    """ Test the correctness of :func:`lib.numbertheory.is_probably_prime`

    :raises AssertionError: if :func:`lib.numbertheory.is_probably_prime` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.is_probably_prime` returns the wrong value
    """

    primes = load_dataset("general", "primes", data_type=int)
    primes = set(primes)
    upper_limit = 1000  # artificial cap to limit the run-time of this test
    max_prime = min(upper_limit, max(primes))
    for n in range(1, max_prime + 1):
        computed_answer = is_probably_prime(n, k=5)
        assert isinstance(computed_answer, is_probably_prime.__annotations__["return"]), "wrong type"
        assert (n in primes) == computed_answer, "wrong answer"


def test_is_probably_prime_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.is_probably_prime` raises a ``TypeError`` for a non-``int`` value for `n`
    (``str``)
    """
    with pytest.raises(TypeError):
        is_probably_prime("2")


def test_is_probably_prime_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.is_probably_prime` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        is_probably_prime(2.4)


def test_miller_rabin_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``TypeError`` for a non-``int`` value for `n` (``str``)
    """
    with pytest.raises(TypeError):
        miller_rabin("2")


def test_miller_rabin_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        miller_rabin(2.4)


def test_miller_rabin_bad_n_too_small():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``ValueError`` for too small a value for `n` """
    with pytest.raises(ValueError):
        miller_rabin(2)


def test_is_probably_prime_bad_k_type_str():
    """ Test that :func:`lib.numbertheory.is_probably_prime` raises a ``TypeError`` for a non-``int`` value for `k`
    (``str``)
    """
    with pytest.raises(TypeError):
        is_probably_prime(2, "14")


def test_is_probably_prime_bad_k_type_float():
    """ Test that :func:`lib.numbertheory.is_probably_prime` raises a ``TypeError`` for a non-``int`` value for `k`
    (``float``)
    """
    with pytest.raises(TypeError):
        is_probably_prime(2, 12.3)


def test_miller_rabin_bad_k_type_str():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``TypeError`` for a non-``int`` value for `k` (``str``)
    """
    with pytest.raises(TypeError):
        miller_rabin(2, "14")


def test_miller_rabin_bad_k_type_float():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``TypeError`` for a non-``int`` value for `k`
    (``float``)
    """
    with pytest.raises(TypeError):
        miller_rabin(2, 12.3)


def test_miller_rabin_bad_k_zero():
    """ Test that :func:`lib.numbertheory.miller_rabin` raises a ``ValueError`` for :math:`k=0` """
    with pytest.raises(ValueError):
        miller_rabin(9, 0)


@pytest.mark.parametrize("siever", [prime_sieve, eratosthenes, segmented_eratosthenes])
def test_prime_sieve_correctness(siever):
    """ Test the correctness of all prime sieves in :func:`lib.numbertheory` including:
        * :func:`lib.numbertheory.primality.prime_sieve`
        * :func:`lib.numbertheory.primality.eratosthenes`
        * :func:`lib.numbertheory.primality.segmented_eratosthenes`

    The integers yielded by these sieves are tested for primality, up to some moderate bound.

    :raises AssertionError: if ``siever(upper_bound)`` yields the wrong type
    :raises AssertionError: if ``siever(upper_bound)`` yields the wrong value
    """

    for p in siever(upper_bound=100):
        computed_answer = is_probably_prime(p)
        # assert isinstance(computed_answer, prime_sieve.__annotations__["return"]), "wrong type"
        assert computed_answer is True, "wrong answer"


@pytest.mark.parametrize("upper_bound", [100, 10 ** 5, 10 ** 8, 10 ** 10])
def test_prime_sieve_correctness_bound(upper_bound):
    """ Test the correctness of all prime sieves in :mod:`lib.numbertheory` including:
        * :func:`lib.numbertheory.primality.prime_sieve`
        * :func:`lib.numbertheory.primality.eratosthenes`
        * :func:`lib.numbertheory.primality.segmented_eratosthenes`

    The integers yielded by these sieves are tested for primality, up to some moderate bound. The parameter
    `upper_bound` is set to various values to ensure all sieves are utilised.

    :raises AssertionError: if :func:`lib.numbertheory.prime_sieve` yields the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.prime_sieve` yields the wrong value
    """

    for p in takewhile(lambda _p: _p <= 100, prime_sieve(upper_bound)):
        computed_answer = is_probably_prime(p)
        # assert isinstance(computed_answer, prime_sieve.__annotations__["return"]), "wrong type"
        assert computed_answer is True, "wrong answer"


def test_prime_sieve_bad_upper_bound_type_str():
    """ Test that :func:`lib.numbertheory.prime_sieve` raises a ``TypeError`` for a non-``int`` value for `upper_bound`
    (``str``)
    """
    with pytest.raises(TypeError):
        prime_sieve("123")


def test_prime_sieve_bad_upper_bound_type_float():
    """ Test that :func:`lib.numbertheory.prime_sieve` raises a ``TypeError`` for a non-``int`` value for `upper_bound`
    (``float``)
    """
    with pytest.raises(TypeError):
        prime_sieve(12.3)


def test_prime_sieve_bad_upper_bound_negative():
    """ Test that :func:`lib.numbertheory.prime_sieve` raises a ``ValueError`` for :math:`upper\\_bound \\lt 0` """
    with pytest.raises(ValueError):
        prime_sieve(-14)


def test_prime_sieve_bad_upper_bound_too_big():
    """ Test that :func:`lib.numbertheory.prime_sieve` raises a ``ValueError`` for
    :math:`\\mbox{upper_bound} \ge 10^{16}`
    """
    too_big = 10 ** 20
    with pytest.raises(ValueError):
        prime_sieve(upper_bound=too_big)


def test_divisors_sieve_correctness_proper():
    """ Test the correctness of the divisors prime sieve :func:`lib.numbertheory.divisors_sieve`

    The divisors yielded by this sieve are tested to ensure that they do divide the given integer :math:`n`, up to some
    moderate bound on :math:`n`. Additionally, it is checked that :math:`1` is included as a proper divisor of
    :math:`n` and that :math:`n` is **not**.

    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` yields the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` yields the wrong value
    """

    for n, divisors in enumerate(divisors_sieve(100, proper=True)):
        for divisor in divisors:
            # assert isinstance(divisor, divisors_sieve.__annotations__["return"]), "wrong type"
            assert (n + 1) % divisor == 0, "non-divisor included by divisors_sieve"
        assert 1 in divisors, "wrong answer: 1 divides everything"
        if n + 1 > 1:
            assert n + 1 not in divisors, "wrong answer: n is not a proper divisor of itself"


def test_divisors_sieve_correctness_not_proper():
    """ Test the correctness of the divisors prime sieve :func:`lib.numbertheory.divisors_sieve`

    The divisors yielded by this sieve are tested to ensure that they do divide the given integer :math:`n`, up to some
    moderate bound on :math:`n`. Additionally, it is checked that :math:`1` is included as a divisor of along with
    :math:`n` itself.

    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` yields the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` yields the wrong value
    """

    for n, divisors in enumerate(divisors_sieve(100, proper=False)):
        for divisor in divisors:
            # assert isinstance(divisor, divisors_sieve.__annotations__["return"]), "wrong type"
            assert (n + 1) % divisor == 0, "non-divisor included by divisors_sieve"
        assert 1 in divisors, "wrong answer: 1 divides everything"
        assert n + 1 in divisors, "wrong answer: n is a divisor of itself"


@pytest.mark.parametrize("upper_bound,proper,aggregate,expected_answer",
                         [(8, True, None, [{1}, {1}, {1}, {1, 2}, {1}, {1, 2, 3}, {1}, {1, 2, 4}]),
                          (7, False, None, [{1}, {1, 2}, {1, 3}, {1, 2, 4}, {1, 5}, {1, 2, 3, 6}, {1, 7}]),
                          (12, True, "count", [1, 1, 1, 2, 1, 3, 1, 3, 2, 3, 1, 5]),
                          (12, False, "count", [1, 2, 2, 3, 2, 4, 2, 4, 3, 4, 2, 6]),
                          (10, True, "sum", [1, 1, 1, 3, 1, 6, 1, 7, 4, 8]),
                          (10, False, "sum", [1, 3, 4, 7, 6, 12, 8, 15, 13, 18])])
def test_divisors_sieve_correctness(upper_bound: int, proper: bool, aggregate: Optional[str],
                                   expected_answer: Union[set, int]):
    """ Test the correctness of :func:`lib.numbertheory.divisors_sieve` using known answer tests

    :param upper_bound: the upper bound of the sieve
    :param proper: whether to consider just proper divisors or not
    :param aggregate: the aggregation function to apply
    :param expected_answer: the expected answer
    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.divisors_sieve` returns the wrong value
    """

    sieve = divisors_sieve(upper_bound=upper_bound, proper=proper, aggregate=aggregate)
    computed_answer = list(sieve)
    # assert isinstance(computed_answer, divisors_sieve.__annotations__["return"]), "wrong type"
    assert computed_answer == expected_answer, "wrong answer"


def test_divisors_sieve_bad_upper_bound_type_str():
    """ Test that :func:`lib.numbertheory.divisors_sieve` raises a ``TypeError`` for a non-``int`` value for
    `upper_bound` (``str``)
    """
    with pytest.raises(TypeError):
        divisors_sieve("123", proper=True)


def test_divisors_sieve_bad_upper_bound_type_float():
    """ Test that :func:`lib.numbertheory.divisors_sieve` raises a ``TypeError`` for a non-``int`` value for
    `upper_bound` (``float``)
    """
    with pytest.raises(TypeError):
        divisors_sieve(12.3, proper=False)


def test_divisors_sieve_bad_upper_bound_negative():
    """ Test that :func:`lib.numbertheory.divisors_sieve` raises a ``ValueError`` for :math:`\\mbox{upper_bound} \\lt 0`
    """
    with pytest.raises(ValueError):
        divisors_sieve(-14, proper=True)


def test_divisors_sieve_bad_aggregate_type_float():
    """ Test that :func:`lib.numbertheory.divisors_sieve` raises a ``TypeError`` for a non-``str`` value for `aggregate`
    (``float``)
    """
    with pytest.raises(TypeError):
        divisors_sieve(10, proper=False, aggregate=3.14)


def test_divisors_sieve_bad_aggregate_invalid():
    """ Test that :func:`lib.numbertheory.divisors_sieve` raises a ``ValueError`` for invalid value of `aggregate` """
    with pytest.raises(ValueError):
        divisors_sieve(10, proper=True, aggregate="foo")


@pytest.mark.parametrize("value,expected_answer", [(0, {0: 1}), (1, {1: 1}), (16, {2: 4})])
def test_factor_correctness(value, expected_answer):
    """ Test the correctness of :func:`lib.numbertheory.factor` with known-answer-tests

    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong value
    """

    computed_value = factor(value)
    # assert isinstance(computed_value, factor.__annotations__["return"]), "wrong type"
    assert computed_value == expected_answer, "wrong value"


@pytest.mark.parametrize("value,expected_answer", [(16, {2: 4}), (35, {5: 1, 7: 1}), (1001, {7: 1, 11: 1, 13: 1})])
def test_factor_correctness_negative(value, expected_answer):
    """ Test the correctness of :func:`lib.numbertheory.factor` with known-answer-tests

    Each `value` is factored. This factorisation is combined with a factor of :math:`-1` to give the expected
    factorisation of :math:`-value`. This negative value is then factored, and the two results are checked for equality.

    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong value
    :raises AssertionError: if :func:`lib.numbertheory.factor` doesn't include a factor of :math:`-1`
    """

    computed_value = factor(value)
    # assert isinstance(computed_value, factor.__annotations__["return"]), "wrong type"
    assert computed_value == expected_answer, "wrong value"
    computed_value_negative = factor(-1 * value)
    # assert isinstance(computed_value_negative, factor.__annotations__["return"]), "wrong type"
    assert -1 not in computed_value.keys(), "missing factor of -1"
    computed_value[-1] = 1
    assert computed_value == computed_value_negative, "wrong value"


def test_factor_correctness_random():
    """ Test the correctness of :func:`lib.numbertheory.factor` with known-answer-tests

    A random integer is built as a product of several small primes (selected with replacement); this random integer has
    a known factorisation. :func:`lib.numbertheory.factor` is used to factor the constructed number and it is compared
    to the known and expected factorisation.

    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.factor` returns the wrong value
    """

    # First, build a random integer as a product of several small primes (selected with replacement)
    primes = load_dataset("general", "primes", data_type=int)
    value, factorisation = 1, Counter()
    while value < 10 ** 6:
        p = choice(primes)
        value *= p
        factorisation[p] += 1
    factorisation = dict(factorisation)

    # Factor the constructed integer and check that it matches the construction
    computed_value = factor(value)
    # assert isinstance(computed_value_negative, factor.__annotations__["return"]), "wrong type"
    assert computed_value == factorisation, "wrong value"


def test_factor_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.factor` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        factor("123")


def test_factor_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.factor` raises a ``TypeError`` for a non-``int`` value for `n` (``float``) """
    with pytest.raises(TypeError):
        factor(145.678)


@pytest.mark.parametrize("square_free", [6, 35, 77])
def test_perfect_power_exponent_correctness_square_free(square_free):
    """ Test the correctness of :func:`lib.numbertheory.perfect_power_exponent` for square-free inputs

    :raises AssertionError: if :func:`lib.numbertheory.perfect_power_exponent` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.perfect_power_exponent` returns the wrong value
    """

    computed_value = perfect_power_exponent(square_free)
    assert isinstance(computed_value, perfect_power_exponent.__annotations__["return"]), "wrong type"
    assert computed_value == 1, "wrong value"


@pytest.mark.parametrize("square_free", [6, 35, 77])
def test_perfect_power_exponent_correctness_not_square_free(square_free):
    """ Test the correctness of :func:`lib.numbertheory.perfect_power_exponent` for **non** square-free inputs

    :raises AssertionError: if :func:`lib.numbertheory.perfect_power_exponent` returns the wrong type
    :raises AssertionError: if :func:`lib.numbertheory.perfect_power_exponent` returns the wrong value
    """

    exponent = randint(2, 10)  # force each square-free number to be a perfect power
    computed_value = perfect_power_exponent(square_free ** exponent)
    assert isinstance(computed_value, perfect_power_exponent.__annotations__["return"]), "wrong type"
    assert computed_value == exponent, "wrong value"


def test_perfect_power_exponent_bad_n_type_str():
    """ Test that :func:`lib.numbertheory.perfect_power_exponent` raises a ``TypeError`` for a non-``int`` value for `n`
    (``str``)
    """
    with pytest.raises(TypeError):
        perfect_power_exponent("123")


def test_perfect_power_exponent_bad_n_type_float():
    """ Test that :func:`lib.numbertheory.perfect_power_exponent` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        perfect_power_exponent(145.678)


def test_perfect_power_exponent_bad_n_zero():
    """ Test that :func:`lib.numbertheory.perfect_power_exponent` raises a ``ValueError`` for :math:`n=0` """
    with pytest.raises(ValueError):
        perfect_power_exponent(0)
