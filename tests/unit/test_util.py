"""
:mod:`tests.unit.test_util` -- Unit Tests
=========================================

.. module:: tests.unit.test_util
   :synopsis: Unit tests for the lib.util module.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

import pytest

from lib.util import wrap, load_dataset


def test_wrap_correctness():
    """ Test the correctness of :func:`lib.util.wrap` by examining outputs for expected properties """
    long_line = "I'm a long line, but how long, really, is a line?"
    long_para = 400 * long_line
    m, n = 4, 120
    wrapped_para = wrap(long_para, m, n)
    assert isinstance(wrapped_para, str), "wrong type"
    for line in wrapped_para.split("\n"):
        assert isinstance(line, str), "wrong type"
        assert line.startswith(m * " "), "incorrect white-space padding at front of line"
        assert len(line) <= n, "line is too long"


def test_wrap_bad_para_type_float():
    """ Test that :func:`lib.util.wrap` raises a ``TypeError`` for a non-``int`` value for `para` (``float``) """
    with pytest.raises(TypeError):
        wrap(3.14, 4, 80)


def test_wrap_bad_m_type_str():
    """ Test that :func:`lib.util.wrap` raises a ``TypeError`` for a non-``int`` value for `m` (``str``) """
    with pytest.raises(TypeError):
        wrap("paragraph I be...", "bad", 80)


def test_wrap_bad_m_type_float():
    """ Test that :func:`lib.util.wrap` raises a ``TypeError`` for a non-``int`` value for `m` (``float``) """
    with pytest.raises(TypeError):
        wrap("paragraph I be...", 3.14, 80)


def test_wrap_bad_m_negative():
    """ Test that :func:`lib.util.wrap` raises a ``ValueError`` for a :math:`m \\lt 0` """
    with pytest.raises(ValueError):
        wrap("paragraph I be...", -14, 80)


def test_wrap_bad_n_type_str():
    """ Test that :func:`lib.util.wrap` raises a ``TypeError`` for a non-``int`` value for `n` (``str``) """
    with pytest.raises(TypeError):
        wrap("paragraph I be...", 4, "bad")


def test_wrap_bad_n_type_float():
    """ Test that :func:`lib.util.wrap` raises a ``TypeError`` for a non-``int`` value for `n` (``float``) """
    with pytest.raises(TypeError):
        wrap("paragraph I be...", 4, 3.14)


def test_wrap_bad_n_zero():
    """ Test that :func:`lib.util.wrap` raises a ``ValueError`` for :math:`n = 0` """
    with pytest.raises(ValueError):
        wrap("paragraph I be...", 4, 0)


def test_load_dataset_correctness():
    """ Test the correctness of :func:`lib.util.load_dataset` by examining datatypes of outputs """
    for elt in load_dataset("general", "primes", data_type=int):
        assert isinstance(elt, int), "wrong type"


def test_load_dataset_bad_root_type_float():
    """ Test that :func:`lib.util.load_dataset` raises a ``TypeError`` for a non-``str`` value for `root` (``float``)
    """
    with pytest.raises(TypeError):
        load_dataset(3.14, "primes")


def test_load_dataset_bad_root_missing():
    """ Test that :func:`lib.util.load_dataset` raises a ``FileNotFoundError`` for a non-existent directory `root` """
    with pytest.raises(FileNotFoundError):
        load_dataset("i_dont_exist", "primes")


def test_load_dataset_bad_dataset_type_float():
    """ Test that :func:`lib.util.load_dataset` raises a ``TypeError`` for a non-``str`` value for `dataset` (``float``)
    """
    with pytest.raises(TypeError):
        load_dataset("general", 3.14)


def test_load_dataset_bad_dataset_missing():
    """ Test that :func:`lib.util.load_dataset` raises a ``FileNotFoundError`` for a non-existent file `dataset` """
    with pytest.raises(FileNotFoundError):
        load_dataset("general", "3.14")


def test_load_dataset_bad_separator_type_float():
    """ Test that :func:`lib.util.load_dataset` raises a ``TypeError`` for a non-``str`` value for `separator`
        (``float``)
    """
    with pytest.raises(TypeError):
        load_dataset("general", "primes", separator=12.34)


def test_load_dataset_bad_data_type():
    """ Test that :func:`lib.util.load_dataset` raises a ``TypeError`` for a non-``type`` value for `data_type`
        (``int``)
    """
    with pytest.raises(TypeError):
        load_dataset("general", "primes", data_type=7)
