"""
:mod:`tests.unit.test_grouptheory` -- Unit Tests
================================================

.. module:: tests.unit.test_grouptheory
   :synopsis: Unit tests for the lib.grouptheory module.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

import pytest

from lib.grouptheory import multiplicative_order


@pytest.mark.parametrize("a,n,order", [(2, 7, 3), (10, 99, 2), (937, 9008, 562), (2, 11111111111, 6158856)])
def test_multiplicative_order_correctness(a: int, n: int, order: int):
    """ Test the correctness of :func:`lib.grouptheory.multiplicative_order` using known answer tests

    :param a: the base input value
    :param n: the modulus defining the multiplicative group
    :param order: the expected answer, i.e. :math:`ord_n(a)`
    :raises AssertionError: if :func:`lib.grouptheory.multiplicative_order` returns the wrong type
    :raises AssertionError: if :func:`lib.grouptheory.multiplicative_order` returns the wrong value
    """

    computed_answer = multiplicative_order(a, n)
    assert isinstance(computed_answer, multiplicative_order.__annotations__["return"]), "wrong type"
    assert computed_answer == order, "wrong answer"


def test_multiplicative_order_bad_a_type_str():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``TypeError`` for a non-``int`` value for `a`
    (``str``)
    """
    with pytest.raises(TypeError):
        multiplicative_order("123", 10)


def test_multiplicative_order_bad_a_type_float():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``TypeError`` for a non-``int`` value for `a`
    (``float``)
    """
    with pytest.raises(TypeError):
        multiplicative_order(123.0, 10)


def test_multiplicative_order_bad_n_type_str():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``TypeError`` for a non-``int`` value for `n`
    (``str``)
    """
    with pytest.raises(TypeError):
        multiplicative_order(2, "123")


def test_multiplicative_order_bad_n_type_float():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``TypeError`` for a non-``int`` value for `n`
    (``float``)
    """
    with pytest.raises(TypeError):
        multiplicative_order(2, 123.0)


def test_multiplicative_order_bad_n_zero():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``ValueError`` for a non-positive value for `n`
    """
    with pytest.raises(ValueError):
        multiplicative_order(2, 0)


def test_multiplicative_order_bad_a_n_not_coprime():
    """ Test that :func:`lib.grouptheory.multiplicative_order` raises a ``ValueError`` for non co-prime `a,n` """
    with pytest.raises(ValueError):
        multiplicative_order(2, 4)
