"""
:mod:`tests.validation_test` -- Validation of Implemented Solutions
===================================================================

.. module:: tests.validation_test
   :synopsis: Validation of all implemented solutions.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

import csv
import importlib
import os
import pkgutil
import re
import unittest
from time import time
from typing import Callable, Union

SOLUTION_MODULE_ROOT = "solutions"
SOLUTION_MODULE_PATH = "{}.problem".format(SOLUTION_MODULE_ROOT) + "{}"
DOC_ROOT = "docs"


results = {}  # global, shared, variable to hold the results for all validation tests


def teardown_module(module):
    """ The teardown (module) function is called when all tests in this module are complete

    This function will collect and report on the results of all validation tests. That is, their correctness and their
    run-times. These reports are collated into ordered CSV files, each capturing up to 100 contiguous tests:

    * tests 1-100
    * tests 101-200
    * etc.

    These CSVs will be picked up by other code to ultimately build the documented project. These CSVs will contribute
    to the tables of solutions and their respective run-times.

    :param module: the module under teardown (i.e. :mod:`tests.validation_test`)
    :return: None
    """

    def build_csv(start_problem: int, end_problem: int) -> None:
        """ Helper function to build a CSV for 100 problems in a 10x10 grid """

        # Check that start_problem and end_problem define a block of 100 problems
        assert isinstance(start_problem, int), "start_problem must be an integer"
        assert isinstance(end_problem, int), "end_problem must be an integer"
        assert start_problem % 100 == 1, "start_problem must be 1 modulo 100"
        assert end_problem % 100 == 0, "end_problem must be 0 modulo 100"
        assert start_problem < end_problem, "start_problem must be less than end_problem"
        assert end_problem - start_problem + 1 == 100, "start_problem and end_problem must be 100 apart"

        global results  # map the local results variable to the globally shared variable

        header = [""] + ["***{}**".format(i % 10) for i in range(1, 11)]

        path = os.path.join(DOC_ROOT, "{}-{}.csv".format(start_problem, end_problem))
        with open(path, "w", newline="") as fp:
            # Open a CSV file and create a header row
            cw = csv.writer(fp)
            cw.writerow(header)

            row = []  # temporary list to hold each row as it is populated

            for i in range(start_problem, end_problem + 1):
                # Add the row range to the start of a CSV row
                if i % 10 == 1:
                    row.append("**{} - {}**".format(i, i + 9))

                # Add the results for problem number i to the next cell in the CSV
                try:
                    if results[i]["correct"]:
                        row.append("|tick| :doc:`{:.2f} <solutions/{}>`".format(results[i]["time"], i))
                    else:
                        row.append("|warning| :doc:`{:.2f} <solutions/{}>`".format(results[i]["time"], i))
                except KeyError:
                    # KeyError caused by results[i], i.e. there isn't a solution for problem number i
                    row.append("|cross|")

                # Flush the 10-long row to the CSV file
                if i % 10 == 0:
                    cw.writerow(row)
                    row = []  # blank row for the next one

    # Construct CSVs in blocks of 100 problems
    build_csv(1, 100)
    build_csv(101, 200)
    build_csv(201, 300)
    build_csv(301, 400)
    build_csv(401, 500)
    build_csv(501, 600)
    build_csv(601, 700)


class TestSolutions(unittest.TestCase):
    """ Will contain all registered solutions once :func:`tests.validation_test.register_solutions` is invoked """
    pass


def make_failure(message: str):
    """ Build a test that unconditionally fails, used to flag errors in dynamic test generation

    :param message: the description of the error
    :return: a unit test function
    """

    def test(self):
        self.fail(message)  # unconditionally fail with the provided error message

    return test


def make_tst_function(description: str, problem_number: int, solver: Callable[[None], Union[int, str]],
                      expected_answer: Union[int, str]):
    """ Build a test that computes the answer to the given problem and checks its correctness

    :param description: a label to attach to this test case
    :param problem_number: the Project Euler problem number
    :param solver: the function that computes the answer
    :param expected_answer: the expected answer to this problem
    :return: a unit test function
    """

    def test(self):
        # Compute the answer using the given solver, note the time taken
        t0 = time()
        computed_answer = solver()
        t1 = time()

        # Record the results (correctness and run-time) in the global, shared, variable
        global results
        results[problem_number] = {"correct": computed_answer == expected_answer, "time": t1 - t0}

        # Check that the computed answer matches the expected one
        self.assertEqual(computed_answer, expected_answer, description)

    return test


def register_solutions():
    """ Dynamically load and run each solution present, test the computed answers match expected answers """

    # Dynamically identify all solutions and for each solved problem, check the answer for correctness
    for importer, modname, is_package in pkgutil.iter_modules([SOLUTION_MODULE_ROOT]):
        rem = re.match("^problem(?P<problem_number>\d+)", modname)
        problem_number = int(rem.group("problem_number"))
        test_name = "test_problem_{:03d}".format(problem_number)

        # Load this solution
        try:
            mod = importlib.import_module("{}.{}".format(SOLUTION_MODULE_ROOT, modname))
        except ModuleNotFoundError:
            err_msg = "solution for problem {} doesn't currently exist.".format(problem_number)
            test_func = make_failure(err_msg)
            setattr(TestSolutions, test_name, test_func)
            continue  # cannot run the test

        # Retrieve the expected answer
        try:
            expected_answer = mod.expected_answer
        except AttributeError:
            err_msg = "expected answer for problem {} doesn't currently exist.".format(problem_number)
            test_func = make_failure(err_msg)
            setattr(TestSolutions, test_name, test_func)
            continue  # cannot validate answer without an expected one

        # Check that the expected answer has a value
        if expected_answer is None:
            err_msg = "expected answer for problem {} hasn't been set (i.e. is still None).".format(problem_number)
            test_func = make_failure(err_msg)
            setattr(TestSolutions, test_name, test_func)
            continue  # cannot validate answer without an expected one

        # Register a test function for this solution
        test_func = make_tst_function(test_name, problem_number, mod.solve, mod.expected_answer)
        setattr(TestSolutions, test_name, test_func)
