Project Euler
=============

.. only:: html

   .. image:: https://img.shields.io/badge/license-MIT-blue.svg
      :target: https://bitbucket.org/nekedome/project-euler/src/master/LICENSE
      :alt: MIT Licence

   .. image:: https://readthedocs.org/projects/nekedome-project-euler/badge/?version=latest
      :target: http://nekedome-project-euler.readthedocs.io/en/latest/?badge=latest
      :alt: Documentation Status

   .. image:: https://img.shields.io/bitbucket/pipelines/nekedome/project-euler.svg
      :target: https://bitbucket.org/nekedome/project-euler/addon/pipelines/home#!/
      :alt: C.I. Build Status

   .. image:: https://codecov.io/bb/nekedome/project-euler/branch/master/graph/badge.svg?token=Ti4lh2KRGD
      :target: https://codecov.io/bb/nekedome/project-euler
      :alt: Code Coverage

   .. image:: https://scrutinizer-ci.com/b/nekedome/project-euler/badges/quality-score.png?b=master
      :target: https://scrutinizer-ci.com/b/nekedome/project-euler/code-structure/master
      :alt: Code Quality (Scrutinizer)

   .. image:: https://api.codacy.com/project/badge/Grade/38fad08048884905a494f420e07929bd
      :target: https://www.codacy.com/app/billmaroney101/project-euler?utm_source=nekedome@bitbucket.org&amp;utm_medium=referral&amp;utm_content=nekedome/project-euler&amp;utm_campaign=Badge_Grade
      :alt: Code Quality (Codacy)

   .. image:: https://www.codefactor.io/repository/bitbucket/nekedome/project%20euler/badge
      :target: https://www.codefactor.io/repository/bitbucket/nekedome/project%20euler
      :alt: Code Quality (CodeFactor)

A collection of my solutions to `Project Euler <https://projecteuler.net>`_ challenges. All code is my own work and I
have solved all problems before reviewing others' approaches. On occasion, I have incorporated superior ideas into these
solutions when I have come across something particularly novel/clever.

Overview
########

.. role::  raw-html(raw)
   :format: html

.. figure:: https://projecteuler.net/images/euler_portrait.png
   :align: right
   :alt: here here

According to its website, Project Euler was created by Colin Hughes (a.k.a. euler) in October 2001 as a sub-section on
http://mathschallenge.net

Project Euler is an ever-growing collection of computational problems with a particular mathematical bent, contributed
by community members.

Each problem has a numeric answer, which can be submitted to the website to check its correctness. Doing so provides
access to a forum discussing others' answers to that same problem.

All problems have been designed to adhere to the "one-minute rule". That is, there is an efficient algorithm to solve
each and every problem that will run on a modestly powerful computer in less than one minute. Distributed computing is
not required and your choice of language is irrelevant - if you have found an efficient solution :raw-html:`&#9786;`

Happy coding!

Usage Instructions
##################

This project contains a collection of solutions to Project Euler problems and a series of useful components:

* a library of mathematical functions (and corresponding unit testing)
* an `API <https://en.wikipedia.org/wiki/Application_programming_interface>`_ for solutions to conform to
* a `command-line application <https://en.wikipedia.org/wiki/Command-line_interface>`_ providing high-level functions
  (see below)

This is a Python 3.6 project and utilises a number of extra dependencies as specified in
`requirements.txt <https://bitbucket.org/nekedome/project-euler/src/master/requirements.txt>`_. You can manually
configure a corresponding Python environment, or alternatively deploy a Python
`virtual environment <https://docs.python.org/3/tutorial/venv.html>`_ and use `pip <https://pypi.org/project/pip/>`_ to
install all necessary dependencies.

For example, in Linux, the following steps will deploy a virtual environment with the required dependencies:

.. code-block:: bash

    python3 -m venv env              # create a virtual environment called env
    source env/bin/activate          # enter the virtual environment
    pip install -r requirements.txt  # install all dependencies

You can find detailed usage instructions in the :doc:`synopsis <cli>` for this project, however, here is a short cheat
sheet.

To begin work on a new problem (identified by ``PROBLEM_NUMBER``) use the following:

.. code-block:: bash

    python main.py start PROBLEM_NUMBER

The ``start`` function will scrape the Project Euler website and create boilerplate files to hold your solution
implementation and its associated documentation describing that solution. The problem description will be captured in
this boiler-plating and any associated datafiles that are linked from the problem on the Project Euler website will also
be downloaded into the ``data/problems`` directory.

Each solution is contained in a Python module in the ``solutions`` directory and exposes two key elements:

1. The ``solve()`` function which computes and returns the putative answer
2. The ``expected_answer`` member which contains the expected answer if it is known (``None`` otherwise)

To compute your solution to ``PROBLEM_NUMBER`` use the following:

.. code-block:: bash

    python main.py solve PROBLEM_NUMBER

Finally, you may want to compute and validate all solutions:

.. code-block:: bash

    python main.py validate

My Solutions
############

The following tables provide links to my solutions where they exist. The table indicates which problems I've solved,
which ones compute the correct answer and their run-times in seconds.

.. |tick| image:: tick.png

.. |cross| image:: cross.png

.. |warning| image:: warning.png

.. only:: html

   .. tabs::

      .. tab:: 1-100

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/1-100.csv
            :align: center

      .. tab:: 101-200

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/101-200.csv
            :align: center

      .. tab:: 201-300

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/201-300.csv
            :align: center

      .. tab:: 301-400

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/301-400.csv
            :align: center

      .. tab:: 401-500

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/401-500.csv
            :align: center

      .. tab:: 501-600

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/501-600.csv
            :align: center

      .. tab:: 601-700

         .. csv-table::
            :widths: 11, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9
            :file: docs/601-700.csv
            :align: center

   .. note:: these run-times are measured on whatever build system is employed by
             `Read the Docs <http://docs.readthedocs.io/>`_ running the standard Python 3.6 interpreter,
             `CPython <https://www.python.org/downloads/>`_. All solutions are single-threaded.

   **Solution Run-time Legend**

   .. glossary::

      |tick|
         the answer was computed correctly

      |warning|
         the answer was computed, but was incorrect

      |cross|
         a solution to this problem does not yet exist

.. include:: modules.rst

Indices and Tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2
   :hidden:

   modules
   genindex
   modindex

.. reviewer-meta::
   :written-on: 2018-04-28
   :proofread-on: 2018-07-12
