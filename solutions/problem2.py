"""
Project Euler Problem 2: Even Fibonacci Numbers
===============================================

.. module:: solutions.problem2
   :synopsis: My solution to problem #2.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem2.py>`_.

Problem Statement
#################

Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with :math:`1` and
:math:`2`, the first :math:`10` terms will be:
    :math:`1, 2, 3, 5, 8, 13, 21, 34, 55, 89, \\dots`

By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the
even-valued terms.

Solution Discussion
###################

Simply iterate over the Fibonacci numbers up to four million and sum all the even ones.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem2.py
   :language: python
   :lines: 34-
"""

from itertools import takewhile

from lib.numbertheory import is_even
from lib.sequence import Fibonaccis


def solve():
    """ Compute the answer to Project Euler's problem #2 """
    upper_bound = 4000000
    numbers = [fib_i for fib_i in takewhile(lambda x: x <= upper_bound, Fibonaccis()) if is_even(fib_i)]
    answer = sum(numbers)
    return answer


expected_answer = 4613732
