"""
Project Euler Problem 20: Factorial Digit Sum
=============================================

.. module:: solutions.problem20
   :synopsis: My solution to problem #20.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem20.py>`_.

Problem Statement
#################

:math:`n!` means :math:`n \\times (n - 1) \\times \\dots \\times 3 \\times 2 \\times 1`

| For example, :math:`10! = 10 \\times 9 \\times \\dots \\times 3 \\times 2 \\times 1 = 3628800`,
| and the sum of the digits in the number :math:`10!` is :math:`3 + 6 + 2 + 8 + 8 + 0 + 0 = 27`.

Find the sum of the digits in the number :math:`100!`

Solution Discussion
###################

Build the value of :math:`100!` using Python's arbitrary precision arithmetic and then perform a decimal digit sum on
that result.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem20.py
   :language: python
   :lines: 35-
"""

from lib.digital import digit_sum
from lib.sequence import Factorials


def solve():
    """ Compute the answer to Project Euler's problem #20 """
    target = 100
    factorials = Factorials()
    x = factorials[target]
    answer = digit_sum(x)
    return answer


expected_answer = 648
