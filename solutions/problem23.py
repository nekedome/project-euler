"""
Project Euler Problem 23: Non-Abundant Sums
===========================================

.. module:: solutions.problem23
   :synopsis: My solution to problem #23.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem23.py>`_.

Problem Statement
#################

A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the
sum of the proper divisors of :math:`28` would be :math:`1 + 2 + 4 + 7 + 14 = 28`, which means that :math:`28` is a
perfect number.

A number :math:`n` is called deficient if the sum of its proper divisors is less than :math:`n` and it is called
abundant if this sum exceeds :math:`n`.

As :math:`12` is the smallest abundant number, :math:`1 + 2 + 3 + 4 + 6 = 16`, the smallest number that can be written
as the sum of two abundant numbers is :math:`24`. By mathematical analysis, it can be shown that all integers greater
than :math:`28123` can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any
further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant
numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.

Solution Discussion
###################

The solution can be found with the following algorithm:

1. Identify all abundant numbers
2. Iterate over all integers in :math:`[2, 28123]` and determine if it is a sum of two abundant numbers, accumulate the
   sum of all integers that cannot be expressed as such a sum

Both steps can be optimised with the following approaches.

First, build a divisor sum sieve which can be enumerated to determine abundant numbers (i.e. :math:`n` s.t.
:math:`s(n) \\gt n` where :math:`s(n)` is the sum of the proper divisors of :math:`n`).

.. note:: the use of a divisor sieve will be much quicker than identifying the divisors of each individual :math:`n`.

Then, iterate through :math:`[2, 28123]` and check whether there are two abundant numbers that can sum to the current
candidate.

* Let :math:`n` be the current candidate
* Let :math:`a_i` be the :math:`i^{th}` abundant number in :math:`[2, 28123]`
* For all :math:`a_i`, check whether there exists an :math:`a_j` s.t. :math:`a_i + a_j = n`
* If no :math:`a_i, a_j` exist, accumulate :math:`n` into the final answer

Importantly, by ordering :math:`\\lbrace a_i \\rbrace`, we don't need to check all :math:`a_j` at each step of this
algorithm.

.. math::

    a_i + a_j &= n \\\\
    \\Rightarrow n - a_i &= a_j

Therefore, we only need to check :math:`a_i \\lt n` (otherwise :math:`n - a_i` would be negative) and since :math:`a_i`
is positive, we only need to check :math:`0 \\lt a_j \\lt n`. This means, that we may consider the subset of
:math:`\\lbrace a_k \\rbrace` s.t. :math:`a_k \\le n` for each candidate :math:`n`.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem23.py
   :language: python
   :lines: 73-
"""

from lib.numbertheory import divisors_sieve


def solve():
    """ Compute the answer to Project Euler's problem #23 """

    upper_bound = 28123

    # Step1: Sieve the sum of proper divisors in [2, upper_bound]
    sum_divisors = list(divisors_sieve(upper_bound + 1, proper=True, aggregate="sum"))

    # Step2: Identify integers not expressible as the sum of two abundant numbers, accumulate their sum
    abundant_subspace = set()  # will contain only those a_i that need be considered for each n
    answer = 0
    for n in range(1, upper_bound + 1):
        if sum_divisors[n - 1] > n:
            abundant_subspace.add(n)
        if not any((n - a in abundant_subspace) for a in abundant_subspace):
            # The any operator provides lazy evaluation, terminating on the first satisfied condition
            answer += n  # accumulate sum

    return answer


expected_answer = 4179871
