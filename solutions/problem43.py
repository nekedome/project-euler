"""
Project Euler Problem 43: Sub-String Divisibility
=================================================

.. module:: solutions.problem43
   :synopsis: My solution to problem #43.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem43.py>`_.

Problem Statement
#################

The number, :math:`1406357289`, is a :math:`0` to :math:`9` pandigital number because it is made up of each of the
digits :math:`0` to :math:`9` in some order, but it also has a rather interesting sub-string divisibility property.

Let :math:`d_1` be the :math:`1^{st}` digit, :math:`d_2` be the :math:`2^{nd}` digit, and so on. In this way, we note
the following:

- :math:`d_2 d_3 d_4 = 406` is divisible by :math:`2`
- :math:`d_3 d_4 d_5 = 063` is divisible by :math:`3`
- :math:`d_4 d_5 d_6 = 635` is divisible by :math:`5`
- :math:`d_5 d_6 d_7 = 357` is divisible by :math:`7`
- :math:`d_6 d_7 d_8 = 572` is divisible by :math:`11`
- :math:`d_7 d_8 d_9 = 728` is divisible by :math:`13`
- :math:`d_8 d_9 d_{10} = 289` is divisible by :math:`17`

Find the sum of all :math:`0` to :math:`9` pandigital numbers with this property.

Solution Discussion
###################

Nothing sophisticated here. Simply iterate through all :math:`0` through :math:`9` pandigital numbers and test for the
required divisibility properties. Accumulate the sum of qualifying candidate pandigital numbers.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem43.py
   :language: python
   :lines: 44-
"""

from itertools import permutations

from lib.digital import digits_to_num


def solve():
    """ Compute the answer to Project Euler's problem #43 """

    divisors = [2, 3, 5, 7, 11, 13, 17]

    answer = 0
    for digits in permutations(range(10)):
        for i, divisor in enumerate(divisors):
            z = sum([100 * digits[i + 1] + 10 * digits[i + 2] + digits[i + 3]])
            if z % divisor != 0:
                break  # one of the divisibility constraints doesn't hold, skip over this pandigital
        else:
            answer += digits_to_num(list(digits))
    return answer


expected_answer = 16695334890
