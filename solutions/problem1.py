"""
Project Euler Problem 1: Multiples of 3 and 5
=============================================

.. module:: solutions.problem1
   :synopsis: My solution to problem #1.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem1.py>`_.

Problem Statement
#################

If we list all the natural numbers below :math:`10` that are multiples of :math:`3` or :math:`5`, we get :math:`3, 5, 6`
and :math:`9`. The sum of these multiples is :math:`23`.

Find the sum of all the multiples of :math:`3` or :math:`5` below :math:`1000`.

Solution Discussion
###################

Nothing sophisticated here, explicitly perform the computation.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem1.py
   :language: python
   :lines: 33-
"""


def solve():
    """ Compute the answer to Project Euler's problem #1 """
    num_range = range(1, 1000)
    valid = lambda n: n % 3 == 0 or n % 5 == 0  # number must be a multiple of 3 OR 5
    numbers = [i for i in num_range if valid(i)]
    answer = sum(numbers)
    return answer


expected_answer = 233168
