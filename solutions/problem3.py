"""
Project Euler Problem 3: Largest Prime Factor
=============================================

.. module:: solutions.problem3
   :synopsis: My solution to problem #3.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem3.py>`_.

Problem Statement
#################

The prime factors of :math:`13195` are :math:`5,7,13` and :math:`29`.

What is the largest prime factor of the number :math:`600851475143`?

Solution Discussion
###################

Factor the number :math:`600851475143` and then find the largest prime factor.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem3.py
   :language: python
   :lines: 31-
"""

from lib.numbertheory import factor


def solve():
    """ Compute the answer to Project Euler's problem #3 """
    target = 600851475143
    factors = factor(target)
    answer = max(factors.keys())  # factors = {prime: power}
    return answer


expected_answer = 6857
