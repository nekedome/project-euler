"""
Project Euler Problem 4: Largest Palindrome Product
===================================================

.. module:: solutions.problem4
   :synopsis: My solution to problem #4.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem4.py>`_.

Problem Statement
#################

A palindromic number reads the same both ways. The largest palindrome made from the product of two :math:`2`-digit
numbers is :math:`9009 = 91 \\times 99`.

Find the largest palindrome made from the product of two :math:`3`-digit numbers.

Solution Discussion
###################

Let :math:`x` be one :math:`3`-digit number and let :math:`y` be the other

Observe that the product of two :math:`3`-digit numbers will be either a :math:`5` or :math:`6` -digit number. Further,
if such numbers were palindromes, they could be expressed in base :math:`10` in one of the following two ways:

.. math::

     abcba &=                   & 10000 \\times a &+ 1000 \\times b &+ 100 \\times c &+ 10 \\times b &+ a \\\\
    abccba &= 100000 \\times a +& 10000 \\times b &+ 1000 \\times c &+ 100 \\times c &+ 10 \\times b &+ a \\\\

Where :math:`a,b,c` are decimal digits.

Since we are looking for a maximal product, we will assume that it is a :math:`6`-digit one. Observe that:

.. math::

    abccba &= 100000 \\times a + 10000 \\times b + 1000 \\times c + 100 \\times c + 10 \\times b + a \\\\
    &= 100001 \\times a + 10010 \\times b + 110 \\times c \\\\
    &= 11 \\times (9091 \\times a + 910 \\times b + 10 \\times c)

Since :math:`11 | x \\times y` and :math:`11` is prime then :math:`11 | x` or :math:`11 | y`. Without loss of
generality, assume that :math:`11 | y`.

To solve this problem search through all :math:`3`-digit numbers :math:`x,y \\in \\mathbb{N}` where :math:`11 | y`.
Then, identify all palindromic products :math:`x \\times y`. Find the maximum such :math:`x \\times y`.

.. note:: if the maximal product was a :math:`5`-digit number then an alternative approach would be needed.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem4.py
   :language: python
   :lines: 58-
"""

from lib.digital import is_palindrome


def solve():
    """ Compute the answer to Project Euler's problem #4 """
    answer = 0
    for x in range(100, 1000):  # all three digit decimal numbers
        for y in range(110, 1000, 11):  # all three digit decimal numbers that are divisible by 11
            if is_palindrome(x * y):
                answer = max(answer, x * y)
    return answer


expected_answer = 906609
