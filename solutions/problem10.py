"""
Project Euler Problem 10: Summation Of Primes
=============================================

.. module:: solutions.problem10
   :synopsis: My solution to problem #10.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem10.py>`_.

Problem Statement
#################

The sum of the primes below :math:`10` is :math:`2 + 3 + 5 + 7 = 17`.

Find the sum of all the primes below two million.

Solution Discussion
###################

Simply accumulate the sum of primes up to the limit. Iterating over primes is off-loaded to :mod:`lib.sequence`.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem10.py
   :language: python
   :lines: 31-
"""

from lib.sequence import Primes


def solve():
    """ Compute the answer to Project Euler's problem #10 """
    target = 2000000
    answer = sum(Primes(upper_bound=target))
    return answer


expected_answer = 142913828922
