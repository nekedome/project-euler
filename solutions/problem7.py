"""
Project Euler Problem 7: 10001St Prime
======================================

.. module:: solutions.problem7
   :synopsis: My solution to problem #7.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem7.py>`_.

Problem Statement
#################

By listing the first six prime numbers: :math:`2,3,5,7,11`, and :math:`13`, we can see that the :math:`6^{th}` prime is
:math:`13`.

What is the :math:`10001^{st}` prime number?

Solution Discussion
###################

Iterate over primes using sieving techniques until we reach the :math:`10001^{st}` one.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem7.py
   :language: python
   :lines: 32-
"""

from itertools import islice

from lib.sequence import Primes


def solve():
    """ Compute the answer to Project Euler's problem #7 """
    target = 10001
    primes = Primes(n_primes=target)
    answer = next(islice(primes, target - 1, target))  # skip ahead to the 10001^{st} prime
    return answer


expected_answer = 104743
