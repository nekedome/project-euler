"""
Project Euler Problem 8: Largest Product In A Series
====================================================

.. module:: solutions.problem8
   :synopsis: My solution to problem #8.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem8.py>`_.

Problem Statement
#################

The four adjacent digits in the :math:`1000`-digit number that have the greatest product are
:math:`\\color{red}{9} \\times \\color{red}{9} \\times \\color{red}{8} \\times \\color{red}{9} = 5832`.

.. math::

                     & 73167176531330624919225119674426574742355349194934 \\hookleftarrow \\\\
    \\hookrightarrow & 96983520312774506326239578318016984801869478851843 \\hookleftarrow \\\\
    \\hookrightarrow & 85861560789112949495459501737958331952853208805511 \\hookleftarrow \\\\
    \\hookrightarrow & 12540698747158523863050715693290963295227443043557 \\hookleftarrow \\\\
    \\hookrightarrow & 66896648950445244523161731856403098711121722383113 \\hookleftarrow \\\\
    \\hookrightarrow & 62229893423380308135336276614282806444486645238749 \\hookleftarrow \\\\
    \\hookrightarrow & 30358907296290491560440772390713810515859307960866 \\hookleftarrow \\\\
    \\hookrightarrow & 70172427121883998797908792274921901699720888093776 \\hookleftarrow \\\\
    \\hookrightarrow & 65727333001053367881220235421809751254540594752243 \\hookleftarrow \\\\
    \\hookrightarrow & 52584907711670556013604839586446706324415722155397 \\hookleftarrow \\\\
    \\hookrightarrow & 53697817977846174064955149290862569321978468622482 \\hookleftarrow \\\\
    \\hookrightarrow & 83972241375657056057490261407972968652414535100474 \\hookleftarrow \\\\
    \\hookrightarrow & 821663704844031\\color{red}{9989}0008895243450658541227588666881 \\hookleftarrow \\\\
    \\hookrightarrow & 16427171479924442928230863465674813919123162824586 \\hookleftarrow \\\\
    \\hookrightarrow & 17866458359124566529476545682848912883142607690042 \\hookleftarrow \\\\
    \\hookrightarrow & 24219022671055626321111109370544217506941658960408 \\hookleftarrow \\\\
    \\hookrightarrow & 07198403850962455444362981230987879927244284909188 \\hookleftarrow \\\\
    \\hookrightarrow & 84580156166097919133875499200524063689912560717606 \\hookleftarrow \\\\
    \\hookrightarrow & 05886116467109405077541002256983155200055935729725 \\hookleftarrow \\\\
    \\hookrightarrow & 71636269561882670428252483600823257530420752963450

Find the thirteen adjacent digits in the :math:`1000`-digit number that have the greatest product. What is the value of
this product?

Solution Discussion
###################

We'll simply iterate over all :math:`13`-long sub-strings in a sliding window fashion. For each sub-string, compute the
product of the integers. The maximum of these individuals products is the answer.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem8.py
   :language: python
   :lines: 57-
"""

from functools import reduce
from operator import mul


def solve():
    """ Compute the answer to Project Euler's problem #8 """

    # Build a list of the individual digits as integer objects
    series = """
        73167176531330624919225119674426574742355349194934
        96983520312774506326239578318016984801869478851843
        85861560789112949495459501737958331952853208805511
        12540698747158523863050715693290963295227443043557
        66896648950445244523161731856403098711121722383113
        62229893423380308135336276614282806444486645238749
        30358907296290491560440772390713810515859307960866
        70172427121883998797908792274921901699720888093776
        65727333001053367881220235421809751254540594752243
        52584907711670556013604839586446706324415722155397
        53697817977846174064955149290862569321978468622482
        83972241375657056057490261407972968652414535100474
        82166370484403199890008895243450658541227588666881
        16427171479924442928230863465674813919123162824586
        17866458359124566529476545682848912883142607690042
        24219022671055626321111109370544217506941658960408
        07198403850962455444362981230987879927244284909188
        84580156166097919133875499200524063689912560717606
        05886116467109405077541002256983155200055935729725
        71636269561882670428252483600823257530420752963450
    """
    series = series.replace(" ", "").replace("\n", "")
    integers = [int(character) for character in series]

    # Perform the search through all overlapping m-long subsets
    n = len(integers)
    m = 13
    answer = 0
    for i in range(n - m + 1):
        subset = integers[i:i+m]
        product = reduce(mul, subset, 1)
        answer = max(answer, product)
    return answer


expected_answer = 23514624000
