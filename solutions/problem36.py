"""
Project Euler Problem 36: Double-Base Palindromes
=================================================

.. module:: solutions.problem36
   :synopsis: My solution to problem #36.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem36.py>`_.

Problem Statement
#################

The decimal number, :math:`585 = 10010010012` (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base :math:`10` and base :math:`2`.

.. note:: that the palindromic number, in either base, may not include leading zeros.

Solution Discussion
###################

This solution simply searches through the integer range and identifies values that are palindromic in bases :math:`2`
and :math:`10`. These values are summed to produce the answer.

The one clever component is to only consider odd numbers. Every even integer has :math:`0` as its least significant bit
which is not allowed for the most significant bit. Therefore no even number is a base :math:`2` palindrome.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem36.py
   :language: python
   :lines: 37-
"""

from lib.digital import is_palindrome


def solve():
    """ Compute the answer to Project Euler's problem #36 """
    upper_bound = 1000000
    answer = 0
    palindromes = filter(lambda n: is_palindrome(n, 10) and is_palindrome(n, 2), range(1, upper_bound, 2))
    answer += sum(palindromes)
    return answer


expected_answer = 872187
