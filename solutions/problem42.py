"""
Project Euler Problem 42: Coded Triangle Numbers
================================================

.. module:: solutions.problem42
   :synopsis: My solution to problem #42.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem42.py>`_.

Problem Statement
#################

The :math:`n^{th}` term of the sequence of triangle numbers is given by, :math:`t_n = \\frac{n(n+1)}{2}`; so the first
ten triangle numbers are:

.. math::

    1, 3, 6, 10, 15, 21, 28, 36, 45, 55, \\dots

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we
form a word value. For example, the word value for ``SKY`` is :math:`19 + 11 + 25 = 55 = t_{10}`. If the word value is a
triangle number then we shall call the word a triangle word.

Using `words.txt <https://projecteuler.net/project/resources/p042_words.txt>`_
(right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how
many are triangle words?

Solution Discussion
###################

Nothing sophisticated here, just map each word to the corresponding number and test whether it is a triangular number
and then count them.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem42.py
   :language: python
   :lines: 43-
"""

from lib.sequence import Triangulars
from lib.util import load_dataset


def is_triangle_word(word: str) -> bool:
    """ Check whether `word` is a triangle word

    :param word: the word to test
    :return: whether `word` is a triangle word or not
    """

    mapping = {chr(i): i - ord('A') + 1 for i in range(ord('A'), ord('Z') + 1)}
    word_value = sum([mapping[letter] for letter in word])
    return word_value in Triangulars()


def solve():
    """ Compute the answer to Project Euler's problem #42 """
    words = load_dataset("problems", "p042_words", separator=",")
    words = [word.strip("\"") for word in words]  # strip quotes off each word
    triangle_words = filter(is_triangle_word, words)
    answer = len(list(triangle_words))  # number of triangle words
    return answer


expected_answer = 162
