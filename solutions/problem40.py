"""
Project Euler Problem 40: Champernowne's Constant
=================================================

.. module:: solutions.problem40
   :synopsis: My solution to problem #40.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem40.py>`_.

Problem Statement
#################

An irrational decimal fraction is created by concatenating the positive integers:

.. math::

    0.12345678910\\color{red}{1}112131415161718192021 \\dots

It can be seen that the :math:`12^{th}` digit of the fractional part is :math:`1`.

If :math:`d_n` represents the :math:`n^{th}` digit of the fractional part, find the value of the following expression.

.. math::

    d_1 \\times d_{10} \\times d_{100} \\times d_{1000} \\times d_{10000} \\times d_{100000} \\times d_{1000000}

Solution Discussion
###################

Explicitly building this decimal representation is not clever but will work, a :math:`1000000` digit number is not large
in the scheme of things.

Use Python to build the fractional part as a string of decimal digits and then simply index into the relevant points to
extract each :math:`d_i` needed. Finally, multiply these values to compute the answer.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem40.py
   :language: python
   :lines: 45-
"""

from functools import reduce
from operator import mul


def solve():
    """ Compute the answer to Project Euler's problem #40 """

    upper_limit = 200000  # large enough s.t. the total string length exceeds the highest index

    fractional_string = "".join(["{}".format(i + 1) for i in range(upper_limit)])  # build the string representation

    # Extract the relevant d_i values
    indices = [1, 10, 100, 1000, 10000, 100000, 1000000]
    digits = [fractional_string[i - 1] for i in indices]  # Python using 0-based indices

    # Multiply each d_i together to get the answer
    answer = reduce(mul, map(int, digits))

    return answer


expected_answer = 210
