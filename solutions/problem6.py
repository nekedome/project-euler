"""
Project Euler Problem 6: Sum Square Difference
==============================================

.. module:: solutions.problem6
   :synopsis: My solution to problem #6.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem6.py>`_.

Problem Statement
#################

The sum of the squares of the first ten natural numbers is,
    :math:`1^2 + 2^2 + \\dots + 10^2 = 385`

The square of the sum of the first ten natural numbers is,
    :math:`(1 + 2 + \\dots + 10)^2 = 55^2 = 3025`

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is
:math:`3025 - 385 = 2640`.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

Solution Discussion
###################

Simply iteratively build and sum the components of the two sequences:

* sum of the squares
* square of the sums

for the natural numbers :math:`1,2,\\dots,100`

Return the absolute value of the difference of these two sums.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem6.py
   :language: python
   :lines: 45-
"""

from math import fabs


def solve():
    """ Compute the answer to Project Euler's problem #6 """
    upper_bound = 100
    sum_of_squares = 0
    square_of_sums = 0
    for i in range(1, upper_bound + 1):
        sum_of_squares += i * i
        square_of_sums += i
    square_of_sums = square_of_sums * square_of_sums
    answer = int(fabs(square_of_sums - sum_of_squares))
    return answer


expected_answer = 25164150
