"""
Project Euler Problem 16: Power Digit Sum
=========================================

.. module:: solutions.problem16
   :synopsis: My solution to problem #16.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem16.py>`_.

Problem Statement
#################

:math:`2^{15} = 32768` and the sum of its digits is :math:`3 + 2 + 7 + 6 + 8 = 26`.

What is the sum of the digits of the number :math:`2^{1000}`?

Solution Discussion
###################

Simply compute :math:`2^{1000}` using Python's arbitrary precision arithmetic and then compute the digital sum.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem16.py
   :language: python
   :lines: 31-
"""

from lib.digital import digit_sum


def solve():
    """ Compute the answer to Project Euler's problem #16 """
    target = 2 ** 1000
    answer = digit_sum(target)
    return answer


expected_answer = 1366
