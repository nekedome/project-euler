"""
Project Euler Problem 21: Amicable Numbers
==========================================

.. module:: solutions.problem21
   :synopsis: My solution to problem #21.

The source code for this problem can be
`found here <https://bitbucket.org/nekedome/project-euler/src/master/solutions/problem21.py>`_.

Problem Statement
#################

Let :math:`d(n)` be defined as the sum of proper divisors of :math:`n` (numbers less than :math:`n` which divide evenly
into :math:`n`).

If :math:`d(a) = b` and :math:`d(b) = a`, where :math:`a \\neq b`, then :math:`a` and :math:`b` are an amicable pair and
each of :math:`a` and :math:`b` are called amicable numbers.

For example, the proper divisors of :math:`220` are :math:`1, 2, 4, 5, 10, 11, 20, 22, 44, 55` and :math:`110`;
therefore :math:`d(220) = 284`. The proper divisors of :math:`284` are :math:`1, 2, 4, 71` and :math:`142`; so
:math:`d(284) = 220`.

Evaluate the sum of all the amicable numbers under :math:`10000`.

Solution Discussion
###################

Pre-compute :math:`d(n)` for all :math:`n` in our search space and then iterate over :math:`n` searching for
:math:`d(n) = m` and :math:`d(m) = n` where :math:`n \\neq m`. Compute the sum of all such :math:`(n, m)`.

.. note:: the pre-computation of :math:`d(n)` for an increasing :math:`n` can be drastically sped up by using sieving
          techniques. An alternative, but inferior, method would be to find the divisors of increasing values of
          :math:`n` via factorisation.

Solution Implementation
#######################

.. literalinclude:: ../../solutions/problem21.py
   :language: python
   :lines: 44-
"""

from lib.numbertheory.sieve import divisors_sieve


def solve():
    """ Compute the answer to Project Euler's problem #21 """

    # Pre-compute d(n) for all n in [2, 9999]
    target = 10000
    divisor_counts = divisors_sieve(target - 1, proper=True, aggregate="sum")
    d_vals = {n + 1: div_count for n, div_count in enumerate(divisor_counts)}
    del d_vals[1]  # divisors_sieve covers the closed interval [1, target - 1], remove d(1)

    # Identify amicable numbers
    amicable_numbers = set()
    for n, d_n in d_vals.items():
        if d_n in d_vals and d_vals[d_n] == n and n != d_n:
            amicable_numbers.add(n)
            amicable_numbers.add(d_n)

    # Sum all amicable numbers
    answer = sum(amicable_numbers)
    return answer


expected_answer = 31626
