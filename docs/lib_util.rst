``lib.util`` Module
===================

Some useful utility functions that won't generally be used to solve any Project Euler problems but make low-level
contributions to this Python project.

.. automodule:: lib.util
   :members:
   :undoc-members:
   :show-inheritance:
