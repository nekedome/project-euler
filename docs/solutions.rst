``solutions`` Package
=====================

This is the main package for Project Euler and contains a command-line application with a number of different
:doc:`high-level functions <synopsis>`, as well as any solutions I have completed so far for Project Euler problems.

.. toctree::
   :maxdepth: 1
   :glob:

   solutions/*
