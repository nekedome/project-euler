``lib`` Package
===============

This Python package is a general purpose library of functions generally useful in solving Project Euler problems.

.. toctree::
   :maxdepth: 3

   lib_digital.rst
   lib_grouptheory.rst
   lib_numbertheory.rst
   lib_sequence.rst
   lib_util.rst
