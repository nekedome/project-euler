``tests.validation_test`` Module
================================

This module contains all integration tests for the :mod:`euler.solutions` package. In particular, it will compute the
solution for all currently solved problems, and where a known answer exists, it will be checked for correctness.

While this project will only include known answers for problems that have been correctly solved, i.e. have been
successfully submitted to https://projecteuler.net/; this test harness will detect problems introduced by changes to the
core :doc:`lib`.

.. note:: this module has been written to dynamically generate test cases for all solutions at run-time. This means that
          it does **not** need to be modified for any new solution included in the project.

The :class:`tests.validation_test.TestSolutions` class is initially defined to be empty, however, the
:func:`tests.validation_test.register_solutions` function will dynamically add member functions to that class. For each
solution detected at run-time, a test case will be created and added to :class:`tests.validation_test.TestSolutions`
named ``test_problem_X``, ``X`` is a three digit decimal number derived from the corresponding Project Euler problem
number.

.. automodule:: tests.validation_test
   :members:
   :undoc-members:
   :show-inheritance:
