``tests.unit`` Package
======================

This package contains all unit tests for the :doc:`lib`. In particular, it will check the correctness of various
functions using known answer tests and also test for expected errors/exceptions when they are invoked with invalid
parameters.

.. toctree::
   :maxdepth: 2

   test_digital.rst
   test_grouptheory.rst
   test_numbertheory.rst
   test_sequence.rst
   test_util.rst
