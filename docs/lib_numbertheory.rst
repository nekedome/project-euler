``lib.numbertheory`` Package
============================

A collection of various number-theoretic functions covering basis numeric properties, divisors of integers, primality
testing, prime number sieving and integer factorisation.

Where these primitives may have many algorithms (e.g. integer factorisation), a collection of algorithms may be included
but there will be an overarching interface that is designed to be optimised for many inputs. These higher-level
functions will utilise appropriate algorithms depending on the input values.

.. automodule:: lib.numbertheory
   :members:
   :imported-members:
   :undoc-members:
   :show-inheritance:
