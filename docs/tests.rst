``tests`` Package
=================

This package contains testing code for the project. The two major varieties of testing include
`unit testing <https://en.wikipedia.org/wiki/Unit_testing>`_ of the :doc:`lib` and validation testing. The
validation testing will re-compute the answer to a specific Project Euler problem, or all of the problems, and check
that the computed answer matches the expected answer, if this code-base contains the expected answer.

The validation testing can be used to manually test changes to this project, it is also heavily used by automated
processes to maintain the documentation for this project.

.. toctree::
   :maxdepth: 4

   test_unit.rst
   test_validation.rst
