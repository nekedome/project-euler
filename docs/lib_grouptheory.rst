``lib.grouptheory`` Module
==========================

A collection of various group-theoretic functions.

.. automodule:: lib.grouptheory
   :members:
   :undoc-members:
   :show-inheritance:
