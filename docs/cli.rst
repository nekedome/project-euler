``euler`` Module
################

This is the main module for Project Euler, it provides some high-level functionality via a command-line application.

By ``starting`` a new problem, its description will be downloaded into a skeleton Python module which
conforms to the API used in this project; the module will be placed at ``solutions/problemX.py`` for problem ``X``. You
must provide a function ``solve()`` which computes and returns the answer. Once you have successfully submitted the
correct answer to the `Project Euler website <https://projecteuler.net/>`_ you should save that answer into the
``expected_answer`` member. This will be used to check the ongoing correctness of your solution.

While developing and testing your solution, you'll want to run your code. To compute the answer to problem ``X`` using
your solution you would exercise the ``solve`` function.

.. note:: both ``start`` and ``solve`` commands take an optional problem identifier; if this is not provided, you will
          be prompted for one on the standard input.

Finally, you may want to [re-]validate all solutions (to check you haven't broken anything) via the ``validate``
function.

Program Synopsis
****************

.. argparse::
   :module: euler
   :func: _build_args_parser
   :prog: euler.py

.. automodule:: euler
   :members:
   :undoc-members:
   :show-inheritance:
