``lib.digital`` Module
======================

A collection of various functions concerning properties of the digital representation of integers. Where sensible, these
functions may consider an integer in various bases such as binary (:math:`2`), decimal (:math:`10`) or hexadecimal
(:math:`16`).

.. automodule:: lib.digital
   :members:
   :undoc-members:
   :show-inheritance:
