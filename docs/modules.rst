Code Layout
###########

This project consists of the following Python modules/packages:

.. toctree::
   :maxdepth: 1

   cli
   lib
   solutions
   tests

This project is hosted on `BitBucket <https://bitbucket.org/nekedome/project-euler/>`_.
