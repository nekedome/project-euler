"""
:mod:`lib.digital` -- A collection of digital functions
=======================================================

.. module:: lib.digital
   :synopsis: Compute various properties about the digital representation of integers.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from math import floor, log
from typing import List, Union

# Pre-compute some num_digits(x) for x = 10 ** i (floating point errors can yield bad answers at these boundaries)
edge_cases = {10 ** i: i + 1 for i in range(100)}
edge_cases[0] = 1


def digits_of(n: int, base: int = 10) -> List[int]:
    """ Construct the list of digits in :math:`n` for the given `base`

    .. note:: the digits are returned in `big-endian <https://en.wikipedia.org/wiki/Endianness>`_ order.

    :param n: input number
    :param base: the base
    :return: a list of the digits of :math:`n` in the given `base`
    :raises TypeError: if :math:`n` or `base` are not ``int`` variables
    :raises ValueError: if :math:`n` is negative
    :raises ValueError: if `base` is not positive

    >>> digits_of(1078)
    [1, 0, 7, 8]
    >>> digits_of(10, base=2)
    [1, 0, 1, 0]
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n < 0:
        raise ValueError("n must be non-negative")
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if base <= 0:
        raise ValueError("base must be positive")

    # Extract the digits
    digits = []
    while n > 0:
        digits.append(n % base)
        n //= base
    digits.reverse()  # convert to big-endian ordering

    if digits:
        return digits
    else:
        return [0]  # the algorithm above won't explicitly produce a 0 digit for n = 0


def digits_to_num(digits: List[int], base: int = 10) -> int:
    """ Convert the given sequence of `digits` into the integer they represent

    :param digits: the sequence of digits
    :param base:
    :return: the integer they represent
    :raises TypeError: if `base` is not an ``int`` variable
    :raises TypeError: if `digits` is not a list of ``int`` variables
    :raises ValueError: if `base` is not positive
    :raises ValueError: if the elements of the list of `digits` aren't valid for the given `base`
    """

    # Parameter validation
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if base <= 0:
        raise ValueError("base must be positive")
    if not isinstance(digits, list):
        raise TypeError("digits must be a list")
    for elt in digits:
        if not isinstance(elt, int):
            raise TypeError("digits must be a list of integers")
        if not (0 <= elt < base):
            raise ValueError("digits must be in the range [0, base)")

    return sum([base ** (len(digits) - i - 1) * x_i for i, x_i in enumerate(digits)])


def digit_sum(n: int, base: int = 10) -> int:
    """ Compute the digit-sum of :math:`n` in the given `base` (the default is decimal)

    Let :math:`b` be `base`. The digit-sum of :math:`n` is the summation of each digit in :math:`n` when it
    is interpreted in base :math:`b`.

    That is, consider :math:`n=d_l \\times b^l + \\dots + d_1 \\times b^1 + d_0 \\times b^0` for base :math:`b`. Then,
    :math:`\\mbox{digit_sum}(n,b) = \\Sigma_{i=0}^l d_i`

    :param n: digit-sum input
    :param base: base to interpret :math:`n` in
    :return: the digit-sum of :math:`n` in the given `base`
    :raises TypeError: if :math:`n` or `base` are not ``int`` variables
    :raises ValueError: if :math:`n` is negative
    :raises ValueError: if `base` is not positive

    >>> digit_sum(123)
    6  # 1 + 2 + 3 = 6
    >>> digit_sum(7, base=2)
    3  # 1 + 1 + 1 = 3
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n < 0:
        raise ValueError("n must be non-negative")
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if base <= 0:
        raise ValueError("base must be positive")

    return sum(digits_of(n, base))


def num_digits(n: int, base: int = 10) -> int:
    """ Compute the number of digits in :math:`n` in the given :math:`base` (the default is decimal)

    :param n: number to count digits from
    :param base: base to interpret :math:`n` in
    :return: the number of digits in :math:`n` in the given `base`
    :raises TypeError: if :math:`n` or `base` are not ``int`` variables
    :raises ValueError: if :math:`n` is negative
    :raises ValueError: if `base` is not positive

    >>> num_digits(123)
    3  # ||123|| = 3
    >>> num_digits(7, base=2)
    3  # ||111|| = 3
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n < 0:
        raise ValueError("n must be non-negative")
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if base <= 0:
        raise ValueError("base must be positive")

    try:
        return edge_cases[n]
    except KeyError:
        return int(floor(log(n, base)) + 1)


def is_pandigital(n: Union[int, List[int]], d: int, lower: int = 1, base: int = 10) -> bool:
    """ Determine whether :math:`n` is :math:`d`-pandigital (the default base is decimal)

    A :math:`d`-pandigital number :math:`n` contains the digits :math:`1` through :math:`d` precisely once.

    :param n: number(s) to test for :math:`d`-pandigital-ness
    :param d: parameter in :math:`d`-pandigital
    :param lower: re-define pandigital to contain the digits :math:`\\mbox{lower},\\dots,d`
    :param base: consider :math:`n` in the given :math:`base`
    :return: whether :math:`n` is :math:`d`-pandigital or not
    :raises TypeError: if :math:`n` is not an ``int`` variable, or a list of ``int`` variables
    :raises TypeError: if :math:`d`, `lower` or `base` are not ``int`` variables
    :raises ValueError: if :math:`n` is negative, or has negative elements
    :raises ValueError: if :math:`d` is not positive
    :raises ValueError: if `lower` is negative
    :raises ValueError: if `base` is not positive

    .. note:: `n` may be an integer, or a list of integers. If `n` is a list of integers, this function returns ``True``
              if **all** digits in the elements of the list constitute a `d`-pandigital number.

    >>> is_pandigital(123, 3)
    True
    >>> is_pandigital(200, 3)
    False
    >>> is_pandigital(123, 4)
    False
    >>> is_pandigital([1, 25, 43], 5)
    True
    """

    # Parameter validation
    if isinstance(n, int):
        n = [n]
    elif not isinstance(n, list):
        raise TypeError("n must be an integer, or a list of integers")
    elif any(map(lambda x: not isinstance(x, int), n)):
        raise TypeError("n must be an integer, or a list of integers")
    if not isinstance(d, int):
        raise TypeError("d must be an integer")
    if not isinstance(lower, int):
        raise TypeError("lower must be an integer")
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if any(map(lambda x: x < 0, n)):
        raise ValueError("n must be non-negative")
    if d <= 0:
        raise ValueError("d must be positive")
    if lower < 0:
        raise ValueError("lower must be non-negative")
    if base <= 0:
        raise ValueError("base must be positive")

    # Count digits in n
    seen = set()
    for _n in n:
        """ Compute and return a histogram of digits in the integer n when represented in the given base """
        for digit in digits_of(_n, base):
            if digit in seen:
                return False  # already seen this digit, cannot have 2 occurrences!
            else:
                seen.add(digit)

    # Identify deviation in counts from a d-pandigital number
    for digit in range(base):
        if lower <= digit <= d and digit not in seen:
            # Count must be exactly 1 for a d-pandigital tuple
            return False
        elif not (lower <= digit <= d) and digit in seen:
            # Count must be exactly 0 for a d-pandigital tuple
            return False

    return True  # n is d-pandigital tuple/integer


def is_palindrome(n: int, base: int = 10) -> bool:
    """ Determine whether the integer :math:`n` is a palindrome when represented in the given `base` (default decimal)

    This function iteratively removes the first and last digits from :math:`n` if they are equivalent, when represented
    in the given `base`. If :math:`n` is a palindrome, this procedure will leave only one digit (if :math:`n` originally
    had an odd number of digits) or no digits (if :math:`n` originally had an even number of digits). Any other number
    of remaining digits implies that n is **not** a palindrome.

    :param n: number to test for being a palindrome
    :param base: consider :math:`n` in the given `base`
    :return: whether :math:`n` is a palindrome or not
    :raises TypeError: if :math:`n` or `base` are not ``int`` variables
    :raises ValueError: if :math:`n` is negative
    :raises ValueError: if `base` is not positive
    :raises ValueError: if `base` is not supported (i.e. not one of `2, 8, 10, 16`)

    >>> is_palindrome(121)
    True
    >>> is_palindrome(123)
    False
    >>> is_palindrome(5, base=2)
    True
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n < 0:
        raise ValueError("n must be non-negative")
    if not isinstance(base, int):
        raise TypeError("base must be an integer")
    if base <= 0:
        raise ValueError("base must be positive")

    # Represent n in the given base as a string
    str_format = {2: "{:b}", 8: "{:o}", 10: "{:d}", 16: "{:x}"}
    try:
        n_str = str_format[base].format(n)  # encode as a decimal string
    except KeyError:
        raise ValueError("unsupported base, select from {}".format(",".join(map(str, str_format.keys()))))

    # Iteratively remove the first and last digits, if equivalent
    while len(n_str) > 1 and n_str[0] == n_str[-1]:  # the first and last characters are equal
        n_str = n_str[1:-1]  # remove the first and last characters and continue

    return len(n_str) <= 1  # n is a palindrome if there are 0 or 1 characters left at this point
