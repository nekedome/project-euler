"""
:mod:`lib.grouptheory` -- A collection of group-theoretic functions
===================================================================

.. module:: lib.grouptheory
   :synopsis: Compute various group-theoretic results.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from functools import reduce
from math import gcd

from lib.numbertheory import factor


def multiplicative_order(a: int, n: int) -> int:
    """ Find the multiplicative order of :math:`a \\mod n`

    Consider the factorisation of :math:`n = p_1^{e_1} \\times p_2^{e_2} \\times \\dots \\times p_k^{e_k}` and compute
    the order of :math:`a \\mod p_i^{e_i} \\mbox{, } \\forall i \\in [1, k]`.

    This problem can now be solved by applying the
    `Chinese remainder theorem <https://en.wikipedia.org/wiki/Chinese_remainder_theorem>`_. In particular, by combining
    the individual results, :math:`a \\mod p_i^{e_i}`, the Chinese remainder theorem gives :math:`a \\mod n`.

    .. note:: while this approach is sound for large :math:`n`, the use of factorisation and the Chinese remainder
              theorem is counterproductive for relatively small :math:`n`. So, for sufficiently small :math:`n`, a brute
              force search is used instead.

    :param a: the group element
    :param n: the multiplicative group modulus
    :return: :math:`ord_n(a)`
    :raises TypeError: if :math:`a` or :math:`n` are not ``int`` variables
    :raises ValueError: if :math:`n \\le 0`
    :raises ValueError: if :math:`(a,n) \\neq 1`

    >>> multiplicative_order(10, 99)
    2
    """

    def brute_force(_a: int, _n: int) -> int:
        """ Determine the order via brute-force
        :param _a: the input base value
        :param _n: the modulus defining the multiplicative group
        :return: :math:`ord_n(a)`
        """

        k, a_k = 1, _a
        while a_k != 1 and k < _n:
            a_k = (a_k * _a) % _n
            k += 1

        return k

    def piecewise(_a: int, _n: int) -> int:
        """ Compute the order via the Chinese remainder theorem based method
        :param _a: the input base value
        :param _n: the modulus defining the multiplicative group
        :return: :math:`ord_n(a)`
        """

        pieces = (order_mod_prime_power(a, *x) for x in factor(n).items())  # the set of a mod p_i^{e_i}
        return reduce(lambda _a, b: (_a * b) // gcd(_a, b), pieces, 1)  # reduce using the least common multiple

    def order_mod_prime_power(_a: int, p_i: int, e_i: int) -> int:
        """ Compute ord_m(a) where m = p_i^{e_i}
        :param _a: the input base value
        :param p_i: a prime factor of the modulus defining the multiplicative group
        :param e_i: the corresponding power of the prime factor (p_i)
        :return: :math:`ord_m(a)`
        """

        m = p_i ** e_i  # this prime power component of the factorisation of n
        t = (p_i - 1) * (p_i ** (e_i - 1))  # Euler's totient, tau(p_i^{e_i})

        # Lagrange's theorem gives us that ord_m(a) | tau(m), find the smallest divisor q of tau(m) s.t. a^q = 1 mod m
        qs = [1]
        for f in factor(t).items():
            qs = [q * f[0] ** j for j in range(1 + f[1]) for q in qs]
        qs.sort()  # ascending order means we consider the smallest q's first
        for q in qs:
            if pow(_a, q, m) == 1:
                break  # we found the smallest such q

        return q

    # Parameter validation
    if not isinstance(a, int):
        raise TypeError("a must be an integer")
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n <= 0:
        raise ValueError("n must be positive")
    if gcd(a, n) != 1:
        raise ValueError("ord_n(a) is only defined for (a,n) = 1")

    a = a % n  # ensure x is in the range [0, n-1]

    # Choose the algorithm based on the size of n
    algorithm = brute_force if n < 100000 else piecewise

    # Compute the order, and validate that it is (at least a multiple) of the order of a modulo n
    order = algorithm(a, n)
    assert pow(a, order, n) == 1, "the computed ord_n(a) is incorrect!"

    return order
