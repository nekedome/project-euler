"""
:mod:`lib.numbertheory.properties` -- A collection of basic number property functions
=====================================================================================

.. module:: lib.numbertheory.properties
   :synopsis: Various functions related to the basic properties of integers.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from math import sqrt


def is_even(n: int) -> bool:
    """ Determine whether :math:`n` is even

    :param n: the integer to test
    :return: whether :math:`n` is even or not
    :raises TypeError: if :math:`n` is not an ``int`` variable

    >>> is_even(4)
    True
    >>> is_even(7)
    False
    >>> is_even(-2)
    True
    """

    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    return (n & 1) == 0


def is_odd(n: int) -> bool:
    """ Determine whether :math:`n` is odd

    :param n: the integer to test
    :return: whether :math:`n` is odd or not
    :raises TypeError: if :math:`n` is not an ``int`` variable

    >>> is_odd(4)
    False
    >>> is_odd(7)
    True
    >>> is_odd(-2)
    False
    """

    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    return (n & 1) == 1


def is_square(n: int) -> bool:
    """ Determine whether :math:`n` is a perfect square, i.e. :math:`n=m^2` s.t. :math:`\\exists m \\in \\mathbb{Z}`

    :param n: the integer to test
    :return: whether :math:`n` is a perfect square or not
    :raises TypeError: if :math:`n` is not an ``int`` variable

    >>> is_square(9)
    True
    >>> is_square(11)
    False
    """

    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    sqrt_n = round(sqrt(n))  # sqrt(n) rounded to the nearest integer
    return sqrt_n * sqrt_n == n
