"""
:mod:`lib.numbertheory.primality` -- A collection of integer primality functions
================================================================================

.. module:: lib.numbertheory.primality
   :synopsis: Various functions related to integer primality.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from itertools import takewhile
from random import randint
from typing import Optional

from lib.util import load_dataset


def is_probably_prime(n: int, k: int = 20) -> bool:
    """ Determine whether :math:`n` is probably prime

    According to Wikipedia, a `probable prime <https://en.wikipedia.org/wiki/Probable_prime>`_ is:
       *an integer that satisfies a specific condition that is satisfied by all prime numbers, but which is not*
       *satisfied by most composite numbers*

    Put another way, all prime numbers are probable primes but most composite numbers are not probable primes. A
    probabilistic primality testing algorithm takes as input an integer :math:`n` which can be tested for primality and
    produces an answer with a bounded error rate very quickly, even for large :math:`n`. Probabilistic primality testing
    algorithms are often favoured over primality proving algorithms for efficiency reasons, given the error margin can
    be bounded by a vanishingly small threshold.

    This function will apply some very simple tests to determine whether :math:`n` is negative, a small prime, or
    divisible by a small prime. In these cases, :math:`n` is determined to be composite, prime, or composite
    respectively.

    If these tests fail to reveal a conclusive answer, the
    `Miller-Rabin <https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test>`_ probabilistic primality testing
    algorithm is applied for the specified number of iterations :math:`k`.

    :param n: the number to test for primality
    :param k: the number of Miller-Rabin iterations to use, if necessary (default is :math:`20`)
    :return: whether :math:`n` is probably prime or not
    :raises TypeError: if :math:`n` or :math:`k` are not ``int`` variables

    >>> is_probably_prime(123)
    True
    >>> is_probably_prime(13 * 17)
    False

    .. note:: the Miller-Rabin algorithm may incorrectly call a composite "probably prime" with probability no greater
              than :math:`4^{-k}`. The default value of :math:`20` for :math:`k` bounds the error rate to
              :math:`2^{-40}`, although in practice the expected error rate for any individual input is generally far
              lower.
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if not isinstance(k, int):
        raise TypeError("k must be an integer")

    if n <= 1:
        return False  # a prime number must be greater than 1
    if 1 < n < 4:
        return True  # 2 and 3 are prime numbers
    elif n % 2 == 0:
        return False  # even numbers are not prime
    else:
        x = trial_division(n)
        if isinstance(x, bool):
            return x  # found a factor by trial division, definitely not prime
        else:
            return miller_rabin(n, k)  # n has not been positively ruled as a prime or composite, use Miller-Rabin


def trial_division(n: int) -> Optional[bool]:
    """ Perform trial division by small odd integers

    :math:`n` is checked for divisibility by the set of small primes up to some threshold. If :math:`n` is found in the
    set of divisors tested then it is definitely prime, if a divisor is found (not equal to :math:`n`), then :math:`n`
    is definitely composite. If no such divisor is found, then `n` may be prime or composite; further testing is
    required.

    :param n: the integer being tested
    :return: ``True`` if :math:`n` is prime, ``False`` if a prime divisor of :math:`n` was found, otherwise ``None``
    """

    small_primes = load_dataset("general", "primes", data_type=int)  # load the pre-computed primes list

    # Check for equality (n is prime) and divisibility (n is composite)
    for p in takewhile(lambda q: q <= 47, small_primes):
        if n == p:
            return True  # n is prime
        elif n % p == 0:
            return False  # n is composite

    return None  # failed to determine whether n is prime or composite


def miller_rabin(n: int, k: int = 20) -> bool:
    """ The Miller-Rabin probabilistic primality testing algorithm

    :param n: the integer being tested
    :param k: the number of iterations of the Miller-Rabin test
    :return: ``True`` if :math:`n` is probably prime, otherwise ``False``
    :raises TypeError: if :math:`n` or :math:`k` are not ``int`` variables
    :raises ValueError: if :math:`n<4` or :math:`k` is not positive
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if not isinstance(k, int):
        raise TypeError("k must be an integer")
    if n < 4:
        raise ValueError("n must be at least 4 for Miller-Rabin")
    if k < 1:
        raise ValueError("k must be at least 1 for Miller-Rabin")

    # Express n as n = 2^r * d + 1
    d = n - 1
    r = 0
    while d % 2 == 0:
        d //= 2
        r += 1

    # Iterate the Miller-Rabin test through k random witnesses
    for i in range(k):
        a = randint(2, n - 2)  # generate a random witness
        x = pow(a, d, n)

        if x == 1:
            continue  # Fermat's little theorem says a prime satisfies x == 1

        # Search for a witness of compositeness
        for j in range(r - 1):
            if x == n - 1:
                continue  # not a witness, try again
            x = pow(x, 2, n)

        if x != n - 1:
            return False  # definitely a composite

    return True  # probably a prime
