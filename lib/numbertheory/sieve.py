"""
:mod:`lib.numbertheory.sieve` -- A collection of number sieving functions
=========================================================================

.. module:: lib.numbertheory.sieve
   :synopsis: Various sieving algorithms to find primes and divisors.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from math import ceil, sqrt
from typing import Iterator, Optional


def prime_sieve(upper_bound: int) -> Iterator[int]:
    """ Build an iterator over a prime sieve up to a specified bound

    A prime sieve is an efficient way to enumerate primes up to some upper bound. Depending on the size of `upper_bound`
    either the `Sieve of Eratosthenes <https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes>`_ is used directly or its
    segmented variant is.

    Let :math:`n` be `upper_bound`. The memory requirements for the sieve of Eratosthenes is :math:`O(n)` while the
    memory requirements for the segmented sieve of Eratosthenes is :math:`O(m)` for a segment size of :math:`m`. This
    implementation sets :math:`m=\sqrt n` which allows the segmented variant to scale for particularly large values of
    `upper_bound` that may not easily fit into memory for the standard sieve.

    :param upper_bound: the upper bound of the prime sieve
    :return: an iterator to the primes produced by the sieve in ascending order
    :raises TypeError: if `upper_bound` is not an ``int`` variable
    :raises ValueError: if `upper_bound` is not positive
    """

    # Parameter validation
    if not isinstance(upper_bound, int):
        raise TypeError("upper_bound must be an integer")
    if upper_bound < 1:
        raise ValueError("upper_bound must be positive")

    # Select the appropriate algorithm based on the upper bound of the sieve
    if upper_bound < 100000000:  # 10 ** 8
        return eratosthenes(upper_bound)
    elif upper_bound < 10000000000000000:  # 10 ** 16
        return segmented_eratosthenes(upper_bound)
    else:
        raise ValueError("upper_bound is too big")


def eratosthenes(upper_bound: int) -> Iterator[int]:
    """ Build an iterator over primes up to `upper_bound` using the sieve of Eratosthenes

    :param upper_bound: the upper bound of the prime sieve
    :return: an iterator to the primes produced by the sieve in ascending order
    """

    upper_bound += 1  # the algorithm uses an open interval, change to a closed interval

    # Build the sieve mapping candidate primes to a flag indicated their primality. This is initially incorrect, but
    # when the algorithm terminates, will be correct.
    candidates = [True] * upper_bound  # an upper_bound long list of True literals
    candidates[0] = None  # 0 is neither prime nor composite

    # Perform the sieve
    p = 2
    while p < ceil(sqrt(upper_bound)):
        yield p  # we can now be sure that p is prime
        # Leave candidates[p] as True, but set candidates[k*p] for k > 1 to False
        for n in range(p * p, upper_bound, p):
            candidates[n] = False
        p += 1
        while p < upper_bound and not candidates[p]:
            p += 1  # find the next prime p

    while p < upper_bound:
        if candidates[p]:
            yield p
        p += 1


def segmented_eratosthenes(upper_bound: int) -> Iterator[int]:
    """ Build an iterator over primes up to `upper_bound` using the segmented sieve of Eratosthenes

    The segmented sieve computes all primes up to the square root of `upper_bound`. These primes can then be used to
    sieve a sliding window in the order of the square root of `upper_bound` before moving on. In short, for a small
    increase in computational complexity, the memory requirements of a segmented sieve are reduced from
    :math:`O(\\mbox{upper_bound})` to :math:`O(\\sqrt{\\mbox{upper_bound}})` allowing for greater scaling.

    :param upper_bound: the upper bound of the prime sieve
    :return: an iterator to the primes produced by the sieve in ascending order
    """

    upper_bound += 1  # the algorithm uses an open interval, change to a closed interval

    # First, determine the segment size as sqrt(upper_bound), this will result in sqrt(upper_bound) segments
    limit = int(ceil(sqrt(upper_bound)))

    # Now, emit these primes individually while building a list (for repetitive enumeration)
    early_primes = []
    for p in prime_sieve(limit):
        yield p
        early_primes.append(p)

    # Finally, apply the early primes to a sliding window/segmented sieve algorithm
    i = limit
    while i < upper_bound:
        # Build a primality map for the current window [sqrt(upper_bound) * i + 1, sqrt(upper_bound) * (i+1) - 1]
        candidates = {n: True for n in range(i + 1, min(upper_bound, i + limit) + 1)}

        # Sieve out multiples of each prime p p in the current window
        for p in early_primes:
            for m in range(i + p - (i % p), min(upper_bound, i + limit) + 1, p):
                candidates[m] = False

        # Emit the primes identified in the current window
        for n, is_prime in candidates.items():
            if is_prime:
                yield n

        i += limit  # move on to the next window


def divisors_sieve(upper_bound: int, proper: bool, aggregate: Optional[str] = None) -> Iterator[int]:
    """ An iterator over the sets of divisors of :math:`n`, for :math:`n \\in [1, upper\\_bound]`

    The obvious way to generate the divisors of a given integer :math:`n` is to factorise that integer. We can then
    enumerate the product of all possible subsets of this factorisation, giving all divisors of :math:`n`.

    In general, there is nothing wrong with this approach. However, if you require the divisors of a sequence of
    contiguous numbers then sieving will be an overall better approach.

    A prime sieve is an efficient way to enumerate primes up to some upper bound. The same principle can be adopted to
    enumerate not just primality but all divisors for this range of numbers.

    The standard operation of this function allows these divisors to be accessed explicitly. However, it is quite common
    to apply aggregation functions to these divisor sets with the individual divisors being irrelevant. Common
    aggregations have been implemented inside this function and for performance reasons and it is recommended they be
    used instead of implementations in consuming code. The following values for ``aggregation`` are supported:

    * ``'count'`` - the number of the divisors of :math:`n`
    * ``'sum'`` - the sum of the divisors of :math:`n`

    .. note:: the memory requirements for this sieve is :math:`O(upper\\_bound)`.

    :param upper_bound: the upper bound of the divisors sieve
    :param proper: whether to iterate over just proper divisors or not
    :param aggregate: an optional aggregation to apply to the divisors of each :math:`n`
    :return: an iterator of the sets of divisors/results of aggregation produced by the sieve in ascending order
    :raises TypeError: if `upper_bound` is not an ``int`` variable
    :raises ValueError: if `upper_bound` is not positive
    :raises TypeError: if `aggregate` is not None or a ``str`` variable
    :raises ValueError: if a ``str``-valued `aggregate` is not ``'count'`` or ``'sum'``

    >>> for n, divisors in enumerate(divisors_sieve(8, proper=False)):
    >>>     print('The divisors of {} are {}.'.format(n + 1, divisors))
    The divisors of 1 are {1}.
    The divisors of 2 are {1, 2}.
    The divisors of 3 are {1, 3}.
    The divisors of 4 are {1, 2, 4}.
    The divisors of 5 are {1, 5}.
    The divisors of 6 are {1, 2, 3, 6}.
    The divisors of 7 are {1, 7}.
    The divisors of 8 are {8, 1, 2, 4}.
    >>> for n, n_divisors in enumerate(divisors_sieve(8, proper=False, aggregate="count")):
    >>>     print('The number of divisors of {} is {}.'.format(n + 1, n_divisors))
    The number of divisors of 1 is 1.
    The number of divisors of 2 is 2.
    The number of divisors of 3 is 2.
    The number of divisors of 4 is 3.
    The number of divisors of 5 is 2.
    The number of divisors of 6 is 4.
    The number of divisors of 7 is 2.
    The number of divisors of 8 is 4.
    >>> for n, divisor_sum in enumerate(divisors_sieve(8, proper=False, aggregate="sum")):
    >>>     print('The sum of the divisors of {} is {}.'.format(n + 1, divisor_sum))
    The sum of the divisors of 1 is 1.
    The sum of the divisors of 2 is 3.
    The sum of the divisors of 3 is 4.
    The sum of the divisors of 4 is 7.
    The sum of the divisors of 5 is 6.
    The sum of the divisors of 6 is 12.
    The sum of the divisors of 7 is 8.
    The sum of the divisors of 8 is 15.
    """

    # Parameter validation
    if not isinstance(upper_bound, int):
        raise TypeError("upper_bound must be an integer")
    if upper_bound < 1:
        raise ValueError("upper_bound must be positive")
    if aggregate is not None and not isinstance(aggregate, str):
        raise TypeError("aggregate must be a string or None")

    if aggregate is None:
        return divisors_sieve_iter(upper_bound, proper)
    elif aggregate == "count":
        return divisors_sieve_count_iter(upper_bound, proper)
    elif aggregate == "sum":
        return divisors_sieve_sum_iter(upper_bound, proper)
    else:
        raise ValueError("invalid value for aggregate, must be 'count' or 'sum'")


def divisors_sieve_iter(upper_bound: int, proper: bool) -> Iterator[int]:
    """ Implementation of a divisors sieve, based on the sieve of Eratosthenes

    :param upper_bound: the upper bound of the divisors sieve
    :param proper: whether to iterate over just sets of proper divisors or not
    :return: an iterator of the sets of divisors produced by the sieve in ascending order
    """

    upper_bound += 1  # the algorithm uses an open interval, change to a closed interval

    # We only need to sieve up-to the square-root of upper_bound since if p | n then (n/p) | n and every such p less
    # than the square-root of m has a corresponding (n/p) greater than the square-root of n
    limit = int(ceil(sqrt(upper_bound)))

    # Build the sieve mapping integers in the range [2, upper_bound] to a set of divisors. This is initially incorrect,
    # but when the algorithm terminates, will be correct.
    divisors = [{1, i} for i in range(upper_bound)]  # an upper_bound long list of trivial divisor sets

    # Perform the sieve
    for a in range(2, limit):
        # Leave candidates[p] as True, but set candidates[k*p] for k > 1 to False
        e = 1
        while a ** e < limit:
            b = a ** e
            for c in range(b, upper_bound, b):
                divisors[c].add(b)
                divisors[c].add(c // b)
            e += 1

    # Removing trivial divisors if only proper divisors were asked for
    if proper:
        for i in range(2, upper_bound):  # 1 is still a proper divisor of 1 itself, so start from 2
            divisors[i].remove(i)  # remove the trivial divisor of i from itself

    yield from divisors[1:]  # skip the divisors of 0 as uninteresting


def divisors_sieve_count_iter(upper_bound: int, proper: bool) -> Iterator[int]:
    """ Implementation of a divisors sieve, based on the sieve of Eratosthenes - modified for divisor counts

    :param upper_bound: the upper bound of the divisors sieve
    :param proper: whether to iterate over just proper divisor counts or not
    :return: an iterator of the divisor counts produced by the sieve in ascending order
    """

    upper_bound += 1  # the algorithm uses an open interval, change to a closed interval

    divisors = [0] * upper_bound  # an upper_bound long list of trivial divisor counts

    # Perform the sieve
    for a in range(1, upper_bound):
        for b in range(a, upper_bound, a):
            divisors[b] += 1

    # Removing trivial divisors if only proper divisors were asked for
    if proper:
        divisors = [div_count - 1 for div_count in divisors]
        divisors[1] = 1

    yield from divisors[1:]  # skip the divisors of 0 as uninteresting


def divisors_sieve_sum_iter(upper_bound: int, proper: bool) -> Iterator[int]:
    """ Implementation of a divisors sieve, based on the sieve of Eratosthenes - modified for divisor sums

    :param upper_bound: the upper bound of the divisors sieve
    :param proper: whether to iterate over just proper divisor sums or not
    :return: an iterator of the divisor sums produced by the sieve in ascending order
    """

    upper_bound += 1  # the algorithm uses an open interval, change to a closed interval

    divisors = [0] * upper_bound  # an upper_bound long list of trivial divisor sums

    # Perform the sieve
    for a in range(1, upper_bound):
        for b in range(a, upper_bound, a):
            divisors[b] += a

    # Removing trivial divisors if only proper divisors were asked for
    if proper:
        divisors = [div_count - i for i, div_count in enumerate(divisors)]
        divisors[1] = 1

    yield from divisors[1:]  # skip the divisors of 0 as uninteresting
