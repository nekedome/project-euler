"""
:mod:`lib.numbertheory.factorisation` -- A collection of integer factorisation functions
========================================================================================

.. module:: lib.numbertheory.factorisation
   :synopsis: Various functions related to integer factorisation.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from collections import Counter
from math import ceil, log, sqrt
from typing import Dict, Tuple

from .primality import is_probably_prime
from .properties import is_square


def factor(n: int) -> Dict[int, int]:
    """ Compute the factorisation of :math:`n` given by :math:`n=\\prod_{i} p_i^{e_i}`, :math:`p_i \\in \\mathbb{N}`

    .. note:: a variety of algorithms are used to try to balance the performance of this factorisation function across
              many inputs. These algorithms include
              `detecting prime inputs <https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test>`_,
              `perfect prime powers <https://en.wikipedia.org/wiki/Perfect_power>`_,
              `trial division <https://en.wikipedia.org/wiki/Trial_division>`_ and
              `Fermat's factoring algorithm <https://en.wikipedia.org/wiki/Fermat%27s_factorization_method>`_.

    :param n: the integer to be factored
    :return: the factorisation of :math:`n` as the dictionary {:math:`p_i`: :math:`e_i`}
    :raises TypeError: if :math:`n` is not an ``int`` variable

    >>> factor(7)
    {7: 1}
    >>> factor(88550)
    {2: 1, 7: 1, 5: 2, 11: 1, 23: 1}
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")

    # Apply various factorisation algorithms depending on the value of n
    if n < 0:
        rv = Counter({-1: 1}) + Counter(factor(-n))  # -n = -1 * n
    elif n in [0, 1]:
        rv = Counter({n: 1})  # trivial cases of n = 0, 1
    elif n % 2 == 0:
        # Strip out powers of 2
        r = 0
        while n % 2 == 0:
            n //= 2
            r += 1
        rv = Counter({2: r}) + Counter(factor(n))  # n = 2^r * n'
    elif is_probably_prime(n):
        rv = Counter({n: 1})  # n is a prime => n = n^1 is the factorisation
    elif perfect_power_exponent(n) > 1:
        # n = m^e for some integer m
        exponent = perfect_power_exponent(n)
        partial_factorisation = factor(round(n ** (1.0 / exponent)))  # factor m
        rv = Counter()
        for i in range(exponent):  # n = factor(m)^e
            rv += Counter(partial_factorisation)
    else:
        rv, the_rest = trial_division(n)  # first do some trial division to strip of small prime factors
        rv = Counter(rv)  # convert the dict to a Counter
        rv += Counter(fermat(the_rest))  # finally, apply the Fermat factorisation algorithm

    # Remove any factors of 1 in a non-trivial factorisation
    if len(rv) > 1:
        del rv[1]

    return dict(rv)


def perfect_power_exponent(n: int) -> int:
    """ Determine if :math:`n` is a perfect power and return that power

    That is, compute the greatest :math:`e` s.t. :math:`n=m^{\\frac{1}{e}}` where :math:`m` is an integer.

    .. note:: if :math:`n` is not a perfect power then the returned value of :math:`e` is :math:`1`.

    :param n: the integer to test
    :return: the value of :math:`e`
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises ValueError: if :math:`n \\le 0`

    >>> perfect_power_exponent(2)
    1
    >>> perfect_power_exponent(27)
    3
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n <= 0:
        raise ValueError("n must be positive")

    # Compute e (exponent variable)
    exponent = 1
    for b in range(1, int(ceil(log(n, 2) + 1))):
        a = round(n ** (1.0 / b))
        if a ** b == n:
            exponent = b

    return exponent


def trial_division(n: int) -> Tuple[Dict[int, int], int]:
    """ Factorisation by trial division

    Identify small prime divisors of :math:`n` by trial division. That is, enumerate a series of small primes and test
    :math:`n` for divisibility by each in turn.

    :param n: the integer to factor
    :return: a partial factorisation of :math:`n`, :math:`n` divided by any identified small prime factors

    >>> trial_division(14260)
    ({2: 2, 5: 1}, 713)
    >>> trial_division(104)
    ({2: 3, 13: 1}, 1)
    """

    small_primes = [2, 3, 5, 7, 11, 13]  # used in trial division

    rv = Counter()
    for p in small_primes:
        while n % p == 0:
            rv[p] += 1
            n //= p

    return dict(rv), n


def fermat(n: int) -> Dict[int, int]:
    """ The `Fermat factorisation method <https://en.wikipedia.org/wiki/Fermat%27s_factorization_method>`_

    The Fermat factorisation method attempts to find a difference of squares equal to :math:`n`; that is,
    :math:`n=a^2-b^2` :math:`\\exists a,b \\in \\mathbb{Z}`. When :math:`a,b` are found, this difference of squares can
    be factored by observing that :math:`n=a^2-b^2=(a+b)(a-b)`

    If neither :math:`a+b` nor :math:`a-b` are :math:`1` then this gives a non-trivial factorisation of :math:`n`.

    .. note:: while this method may split :math:`n` into two non-trivial integer parts, they may not each be prime
              numbers. In practice, this (or other) methods must be applied recursively to both :math:`a+b` and
              :math:`a-b`. This function applies the :func:`factor` function to both components which
              should ultimately return the full factorisation of :math:`n`.

    :param n: the integer to factor
    :return: the factorisation of :math:`n`
    """

    a = ceil(sqrt(n))
    b2 = a * a - n
    while not is_square(b2):
        a += 1
        b2 = a * a - n
    b = int(sqrt(b2))

    return dict(Counter(factor(a - b)) + Counter(factor(a + b)))
