"""
:mod:`lib.numbertheory` -- A collection of number-theoretic functions
=====================================================================

.. module:: lib.numbertheory
   :synopsis: Compute various number-theoretic results.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from .properties import is_even, is_odd, is_square
from .divisors import divisor_count, divisor_sum, divisor_sum_aliquot
from .primality import is_probably_prime
from .sieve import prime_sieve, divisors_sieve
from .factorisation import factor
