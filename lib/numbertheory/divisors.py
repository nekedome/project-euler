"""
:mod:`lib.numbertheory.divisors` -- A collection of functions for divisors of integers
======================================================================================

.. module:: lib.numbertheory.divisors
   :synopsis: Various functions related to the divisors of integers.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from functools import reduce
from operator import mul

import lib.sequence
from .factorisation import factor


def divisor_count(n: int) -> int:
    """ Compute :math:`\sigma_0(n)=\Sigma_{d|n}1`; the number of all divisors of :math:`n`

    :param n: integer input
    :return: :math:`\sigma_0(n)`
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises ValueError: if :math:`n` is not positive

    >>> divisor_count(17)
    2
    >>> divisor_count(10)
    4
    """

    # Parameter validation
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if n <= 0:
        raise ValueError("n must be positive")

    if n == 1:
        return 1  # 1 has just one divisor, itself
    else:  # n > 1
        factorisation = factor(n)
        exponents = factorisation.values()
        product = reduce(mul, [exponent + 1 for exponent in exponents], 1)
        return product


def divisor_sum(n: int) -> int:
    """ Compute :math:`\sigma_1(n)=\Sigma_{d|n}d`; the sum of all divisors of :math:`n`

    :param n: integer input
    :return: :math:`\sigma_1(n)`
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises ValueError: if :math:`n` is not positive

    >>> divisor_sum(17)
    18
    >>> divisor_sum(10)
    18
    """

    return sum(lib.sequence.Divisors(n, proper=False))


def divisor_sum_aliquot(n: int) -> int:
    """ Compute :math:`s(n)=\sigma_1(n)-n`; the sum of all **proper** divisors of :math:`n`

    :param n: integer input
    :return: :math:`s(n)`
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises ValueError: if :math:`n` is not positive

    >>> divisor_sum_aliquot(17)
    1
    >>> divisor_sum_aliquot(10)
    8
    """

    return sum(lib.sequence.Divisors(n, proper=True))
