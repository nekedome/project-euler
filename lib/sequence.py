"""
:mod:`lib.sequence` -- A collection of sequence abstractions
============================================================

.. module:: lib.sequence
   :synopsis: Iterate over various well-known numeric sequences.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from collections import abc
from functools import reduce
from itertools import permutations, dropwhile
from math import ceil, log, sqrt
from operator import mul

import lib.numbertheory


class Factorials(abc.Iterator):
    """ The `factorial numbers <https://en.wikipedia.org/wiki/Factorial>`_: :math:`\\lbrace n! \\rbrace_{n=0}^{\\infty}`
    where :math:`n! = n \\times (n-1) \\times \\dots \\times 1` and :math:`0! = 1`

    .. note:: checking whether an integer :math:`x` is a factorial number has computational complexity
              :math:`O(\\log x)`

    :raises TypeError: if this sequence is indexed by a variable that is not of type ``int``
    :raises ValueError: if this sequence is indexed by an ``int`` variable with a negative value
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(itertools.takewhile(lambda x: x < 1000, Factorials()))
    [1, 1, 2, 6, 24, 120, 720]
    >>> 24 in Factorials()
    True
    >>> 25 in Factorials()
    False
    """

    def __init__(self):
        self.n = 0
        self.n_factorial = 1

    def __next__(self) -> int:
        rv = self.n_factorial
        self.n += 1
        self.n_factorial *= self.n
        return rv

    def __getitem__(self, item) -> int:
        if not isinstance(item, int):
            raise TypeError("item must be an integer")
        if item < 0:
            raise ValueError("item must be non-negative")

        seq = iter(Factorials())
        for n in range(item + 1):
            elt = next(seq)
        return elt

    def __contains__(self, item: int) -> bool:
        """ Test whether item is a member of this sequence

        Since the factorial numbers form an increasing sequence, membership can be tested by iterating through the
        sequence until item is reached/exceeded. Either item is encountered or it is exceeded. In the later case, since
        the sequence is increasing, we know it will not appear further into the sequence. The search can now stop.

        The factorial numbers grow [super-]exponentially, therefore this algorithm has cost logarithmic in item
        """

        if not isinstance(item, (int, float)):  # float allows ints stored as floats to be checked (e.g. item = 10.0)
            raise TypeError("item must be an int/float")

        for elt in dropwhile(lambda f_n: f_n < item, Factorials()):  # skip over elements < item
            return elt == item  # elt is the first element >= item


class Fibonaccis(abc.Iterator):
    """ The `Fibonacci numbers <https://en.wikipedia.org/wiki/Fibonacci_number>`_:
    :math:`\\lbrace F_n \\rbrace_{n=0}^{\\infty}`
    where :math:`F_n = F_{n-1} + F_{n-2} \\mbox{ } \\forall n \\ge 2` and :math:`F_0 = 0, F_1 = 1`

    .. note:: checking whether an integer :math:`x` is a Fibonacci number has computational complexity
              :math:`O(\\log x)`

    :raises TypeError: if this sequence is indexed by a variable that is not of type ``int``
    :raises ValueError: if this sequence is indexed by an ``int`` variable with a negative value
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(itertools.takewhile(lambda x: x < 50, Fibonaccis()))
    [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
    >>> 20 in Fibonaccis()
    False
    >>> 21 in Fibonaccis()
    True
    """

    def __init__(self):
        self.fib0 = 0
        self.fib1 = 1

    def __next__(self) -> int:
        rv = self.fib0
        self.fib0, self.fib1 = self.fib1, self.fib0 + self.fib1
        return rv

    def __getitem__(self, item) -> int:
        if not isinstance(item, int):
            raise TypeError("item must be an integer")
        if item < 0:
            raise ValueError("item must be non-negative")

        seq = iter(Fibonaccis())
        for n in range(item + 1):
            elt = next(seq)
        return elt

    def __contains__(self, item: int) -> bool:
        """ Test whether item is a member of this sequence

        Since the Fibonacci numbers form an increasing sequence, membership can be tested by iterating through the
        sequence until item is reached/exceeded. Either item is encountered or it is exceeded. In the later case, since
        the sequence is increasing, we know it will not appear further into the sequence. The search can now stop.

        The Fibonacci numbers grow exponentially, therefore this algorithm has cost logarithmic in item
        """

        if not isinstance(item, (int, float)):  # float allows ints stored as floats to be checked (e.g. item = 10.0)
            raise TypeError("item must be an int/float")

        for elt in dropwhile(lambda f_n: f_n < item, Fibonaccis()):  # skip over elements < item
            return elt == item  # elt is the first element >= item


class Figurates(abc.Sequence):
    """ An abstract base class for `figurate numbers <https://en.wikipedia.org/wiki/Figurate_number>`_

    .. warning:: this class cannot be instantiated directly, use one of the concrete children classes:

                 * :class:`lib.sequence.Triangulars`
                 * :class:`lib.sequence.Pentagonals`
                 * :class:`lib.sequence.Hexagonals`

    .. warning:: this class is derived from ``collections.abc.Sequence``, so it must implement a ``__len__`` method to
                 allow any instance to be evaluated in the standard ``len`` function. However, figurate sequences are
                 infinite. By Python convention, this class (and all of its children) will raise a ``TypeError`` when
                 evaluated by ``len``.
    """

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self) -> int:
        self.i += 1
        return self._f(self.i)

    def __len__(self):
        raise TypeError("object of type '{}' has infinite length".format(type(self)))

    def __getitem__(self, item: int) -> int:
        if not isinstance(item, int):
            raise TypeError("item must be an integer")
        return self._f(item)

    @staticmethod
    def __contains__(item: int) -> bool:
        raise NotImplementedError

    @staticmethod
    def _f(n: int) -> int:
        """ The :math:`n^{th}` element of this figurate sequence """
        raise NotImplementedError


class Triangulars(Figurates):
    """ The `triangular numbers <https://en.wikipedia.org/wiki/Triangular_numbers>`_:
    :math:`\\lbrace T_n \\rbrace_{n=1}^{\\infty}` where :math:`T_n = \\frac{n (n + 1)}{2}`

    .. note:: checking whether an integer :math:`x` is triangular has computational complexity :math:`O(\\log \\log x)`

    :raises TypeError: if this sequence is indexed by a variable that is not of type ``int``
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(itertools.takewhile(lambda x: x < 50, Triangulars()))
    [1, 3, 6, 10, 15, 21, 28, 36, 45]
    >>> len(Triangulars())
    TypeError: object of type '<class 'lib.sequence.Triangulars'>' has infinite length
    >>> 10 in Triangulars()
    True
    >>> 20 in Triangulars()
    False
    """

    @staticmethod
    def __contains__(item: int) -> bool:
        if not isinstance(item, (int, float)):
            raise TypeError("item must be an int/float")

        return lib.numbertheory.is_square(8 * item + 1)

    @staticmethod
    def _f(n: int) -> int:
        """ The :math:`n^{th}` triangular number """
        return n * (n + 1) // 2


class Pentagonals(Figurates):
    """ The `pentagonal numbers <https://en.wikipedia.org/wiki/Pentagonal_number>`_:
    :math:`\\lbrace p_n \\rbrace_{n=1}^{\\infty}` where :math:`p_n = \\frac{n (3n - 1)}{2}`

    .. note:: checking whether an integer :math:`x` is pentagonal has computational complexity :math:`O(\\log \\log x)`

    :raises TypeError: if this sequence is indexed by a variable that is not of type ``int``
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(itertools.takewhile(lambda x: x < 100, Pentagonals()))
    [1, 5, 12, 22, 35, 51, 70, 92]
    >>> len(Pentagonals())
    TypeError: object of type '<class 'lib.sequence.Pentagonals'>' has infinite length
    >>> 11 in Pentagonals()
    False
    >>> 22 in Pentagonals()
    True
    """

    @staticmethod
    def __contains__(item: int) -> bool:
        if not isinstance(item, (int, float)):
            raise TypeError("item must be an int/float")

        if lib.numbertheory.is_square(24 * item + 1):
            return sqrt(24 * item + 1) % 6 == 5

        return False

    @staticmethod
    def _f(n: int) -> int:
        """ The :math:`n^{th}` pentagonal number """
        return n * (3 * n - 1) // 2


class Hexagonals(Figurates):
    """ The `hexagonal numbers <https://en.wikipedia.org/wiki/Hexagonal_number>`_:
    :math:`\\lbrace h_n \\rbrace_{n=1}^{\\infty}` where :math:`h_n = n (2n - 1)`

    .. note:: checking whether an integer :math:`x` is hexagonal has computational complexity :math:`O(\\log \\log x)`

    :raises TypeError: if this sequence is indexed by a variable that is not of type ``int``
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(itertools.takewhile(lambda x: x < 100, Hexagonals()))
    [1, 6, 15, 28, 45, 66, 91]
    >>> len(Hexagonals())
    TypeError: object of type '<class 'lib.sequence.Hexagonals'>' has infinite length
    >>> 15 in Hexagonals()
    True
    >>> 30 in Hexagonals()
    False
    """

    @staticmethod
    def __contains__(item: int) -> bool:
        if not isinstance(item, (int, float)):
            raise TypeError("item must be an int/float")

        if lib.numbertheory.is_square(8 * item + 1):
            return sqrt(8 * item + 1) % 4 == 3

        return False

    @staticmethod
    def _f(n: int) -> int:
        """ The :math:`n^{th}` hexagonal number """
        return n * (2 * n - 1)


class Pandigitals(abc.Iterator):
    """ The `pandigital numbers <https://en.wikipedia.org/wiki/Pandigital_number>`_

    A decimal :math:`n`-pandigital number contains the digits :math:`1` through :math:`d` precisely once.

    For example, :math:`51243` is a :math:`5`-pandigital number while :math:`61` is not.

    :param n: the pandigital parameter :math:`n`
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises ValueError: if :math:`n \le 0`

    >>> list(Pandigitals(3))
    [123, 132, 213, 231, 312, 321]
    >>> len(Pandigitals(6))
    720
    """

    def __init__(self, n: int) -> None:
        if not isinstance(n, int):
            raise TypeError("n must be an integer")
        if n <= 0:
            raise ValueError("n must be positive")

        self.n = n
        self.digits = permutations(range(1, self.n + 1))

    def __next__(self) -> int:
        digits = next(self.digits)
        pandigital_number = sum([digit * 10 ** (self.n - 1 - i) for i, digit in enumerate(digits)])
        return pandigital_number

    def __len__(self):
        return Factorials()[self.n]


class Divisors(abc.Iterator):
    """ All positive `integer divisors <https://en.wikipedia.org/wiki/Divisor>`_ of :math:`n`, i.e.
    :math:`\\lbrace d_i \\rbrace \\mbox{ } \\forall d_i \\in \\mathbb{N} \\mbox{ s.t. } d_i \\vert n`

    This sequence can be used to iterate over all divisors of :math:`n`, or just the proper divisors of :math:`n` by
    providing a suitable value for the `proper` parameter.

    .. note:: the proper divisors of :math:`n` do not include :math:`n` itself.

    :param n: the target integer
    :param proper: whether to iterate over just proper divisors or not
    :raises TypeError: if :math:`n` is not an ``int`` variable
    :raises TypeError: if `proper` is not a ``bool`` variable
    :raises ValueError: if :math:`n \\le 0`
    :raises TypeError: if a variable is tested for membership of this sequence that is not of type ``int``/``float``

    >>> list(Divisors(10, proper=True))
    [1, 2, 5]
    >>> list(Divisors(10, proper=False))
    [1, 2, 5, 10]
    >>> len(Divisors(10, proper=True))
    3
    >>> len(Divisors(10, proper=False))
    4
    >>> 5 in Divisors(10, proper=True)
    True
    >>> 7 in Divisors(10, proper=False)
    False
    """

    def __init__(self, n: int, proper: bool):
        if not isinstance(n, int):
            raise TypeError("n must be an integer")
        if not isinstance(proper, bool):
            raise TypeError("proper must be an integer")
        if n <= 0:
            raise ValueError("n must be positive")

        self.n = n
        self.proper = proper

        if self.n == 1:
            self.factorisation = None
            self.iter = self.__divisors_of_1()
        else:
            self.factorisation = lib.numbertheory.factor(n)
            self.iter = self.__divisors_of_n()

    def __next__(self) -> int:
        return next(self.iter)

    def __len__(self):
        if self.n == 1:
            return 0 if self.proper else 1
        else:
            exponents = self.factorisation.values()
            product = reduce(mul, [exponent + 1 for exponent in exponents], 1)
            return (product - 1) if self.proper else product

    def __contains__(self, item: int) -> bool:
        if not isinstance(item, (int, float)):
            raise TypeError("item must be an int/float")
        if self.proper:
            return ((self.n % item) == 0) and self.n != item  # n is not a proper divisor of itself
        else:
            return (self.n % item) == 0

    def __increment_counters(self, counters):
        for p, e in self.factorisation.items():
            if counters[p] == e:
                counters[p] = 0
            else:
                counters[p] += 1
                break
        return counters

    def __divisors_of_1(self):
        if self.proper:
            return
        else:
            yield 1

    def __divisors_of_n(self):
        def build_product(factorisation):
            """ Build the integer corresponding to the given factorisation """
            return reduce(mul, [p ** e for p, e in factorisation.items()], 1)

        counters = {prime_factor: 0 for prime_factor in self.factorisation.keys()}

        while counters != self.factorisation:
            yield build_product(counters)
            counters = self.__increment_counters(counters)

        if not self.proper:
            yield build_product(self.factorisation)


class DivisorsRange(abc.Iterator):
    """ The sequence of the sets of divisors of :math:`n`, for :math:`n \\in [1, upper\\_bound]`

    Each element in this sequence contains all the positive `integer divisors <https://en.wikipedia.org/wiki/Divisor>`_
    of :math:`n`, i.e. :math:`\\lbrace d_i \\rbrace \\mbox{ } \\forall d_i \\in \\mathbb{N} \\mbox{ s.t. } d_i \\vert n`
    for the range :math:`n \\in [1, upper\\_bound]`.

    This sequence can be used to iterate over all divisors of :math:`n`, or just the proper divisors of :math:`n` by
    providing a suitable value for the `proper` parameter.

    .. note:: the proper divisors of :math:`n` do not include :math:`n` itself.

    :param upper_bound: the target integer
    :param proper: whether to iterate over just proper divisors or not
    :raises TypeError: if :math:`upper\\_bound` is not an ``int`` variable
    :raises TypeError: if `proper` is not a ``bool`` variable
    :raises ValueError: if :math:`upper\\_bound \\le 0`

    >>> list(DivisorsRange(10, proper=True))
    [{1}, {1}, {1}, {1, 2}, {1}, {1, 2, 3}, {1}, {1, 2, 4}, {1, 3}, {1, 2, 5}]
    >>> list(DivisorsRange(10, proper=False))
    [{1}, {1, 2}, {1, 3}, {1, 2, 4}, {1, 5}, {1, 2, 3, 6}, {1, 7}, {8, 1, 2, 4}, {1, 3, 9}, {1, 10, 2, 5}]

    .. note:: this sequence may be considered redundant given the existing :mod:`lib.sequence.Divisors` sequence. The
              :mod:`lib.sequence.DivisorsRange` sequence is provided for the case when considering the divisors of a
              range of numbers, and not just a single number. In the case of a contiguous range of inputs, the
              :mod:`lib.sequence.DivisorsRange` will provide superior performance due to its use of sieving techniques.
    """

    def __init__(self, upper_bound: int, proper: bool):
        if not isinstance(upper_bound, int):
            raise TypeError("n must be an integer")
        if not isinstance(proper, bool):
            raise TypeError("proper must be an integer")
        if upper_bound <= 0:
            raise ValueError("n must be positive")

        self.upper_bound = upper_bound
        self.proper = proper

        self.__divisors = None

    def __iter__(self):
        self.__divisors = lib.numbertheory.divisors_sieve(self.upper_bound, self.proper)
        return self

    def __next__(self) -> int:
        return next(self.__divisors)


class Primes(abc.Iterator):
    """ The `prime numbers <https://en.wikipedia.org/wiki/Prime_number>`_: :math:`\\lbrace p_i \\rbrace_{n=1}^{l}` where
    :math:`p_i` is the :math:`i^{th}` prime number and the bound :math:`l` is defined as follows

    .. note:: a prime number :math:`p \\in \\mathbb{N}` is only divisible by itself and :math:`1`.

    This sequence builds an iterator over the prime numbers by use of a
    `sieving algorithm <https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes>`_ and so a finite bound must be specified.
    This bound may be either of:

    * all prime numbers up to, and including, `upper_bound`; or
    * the first `n_primes` prime numbers, in ascending order.

    .. warning:: you must specify precisely one of `upper_bound` or `n_primes`, but not both.

    :param upper_bound: the upper bound of primes in the sequence
    :param n_primes: the number of primes in the sequence
    :raises TypeError: if :math:`upper\\_bound` is not an ``int`` variable
    :raises TypeError: if :math:`n\\_primes` is not an ``int`` variable
    :raises ValueError: if :math:`upper\\_bound \\le 0`
    :raises ValueError: if :math:`n\\_primes \\lt 0`

    >>> list(Primes(upper_bound=10))
    [2, 3, 5, 7]
    >>> list(Primes(n_primes=10))
    [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    """

    def __init__(self, upper_bound: int = None, n_primes: int = None):
        if isinstance(upper_bound, int) and n_primes is None:
            if upper_bound <= 0:
                raise ValueError("upper_bound must be positive")
            self.upper_bound = upper_bound
            self.n_primes = None
        elif isinstance(n_primes, int) and upper_bound is None:
            if n_primes < 0:
                raise ValueError("n_primes must be non-negative")
            if n_primes <= 3:
                self.upper_bound = 5  # this will cover up to the first three primes (2, 3, 5)
            else:
                # Use an approximation: p_n < n * log(n * log(n)), this holds for n > 3
                self.upper_bound = int(ceil(n_primes * log(n_primes * log(n_primes))))
            self.n_primes = n_primes
        else:
            raise TypeError("you must specify precisely one of upper_bound or n_primes")

        self.__primes = None
        self.__i = None

    def __iter__(self):
        self.__primes = lib.numbertheory.prime_sieve(self.upper_bound)
        self.__i = 0
        return self

    def __next__(self) -> int:
        if self.n_primes is not None and self.__i >= self.n_primes:
            # The application has requested a specific number of primes, however, the sieve may be bigger than necessary
            # so explicitly stop the iterator when self.__i == self.n_primes
            raise StopIteration
        else:
            self.__i += 1  # track the number of primes iterated so far
            return next(self.__primes)


class ContinuedFraction(abc.Iterator):
    pass


class SqrtExpansion(ContinuedFraction):
    pass
