"""
:mod:`lib.util` -- A collection of utility functions for this Python project
============================================================================

.. module:: lib.util
   :synopsis: Various utility/helper functions used by this Python project.

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

from functools import lru_cache
import os
from typing import Callable, List, Type, TypeVar, Union

DATA_DIR = "data"  # base directory for datasets

# Python3 typing annotation declarations
DTT = Union[Type[str], Type[int], Type[float]]
DT = TypeVar("DT", str, int, float)


def wrap(para: str, m: int, n: int) -> str:
    """ Wrap a paragraph on a width of :math:`n` characters, indented by :math:`m` spaces

    :param para: the paragraph to wrap on a given width
    :param m: the number of spaces of indentation
    :param n: the maximum line width to work to
    :return: the paragraph wrapped as specified
    :raises TypeError: if `para` is not a ``str`` variable
    :raises TypeError: if :math:`m,n` are not ``int`` variables
    :raises ValueError: if :math:`m \\lt 0`
    :raises ValueError: if :math:`n \\le 0`
    """

    # Parameter validation
    if not isinstance(para, str):
        raise TypeError("para must be a string")
    if not isinstance(m, int):
        raise TypeError("m must be an integer")
    if not isinstance(n, int):
        raise TypeError("n must be an integer")
    if m < 0:
        raise ValueError("m must be non-negative")
    if n <= 0:
        raise ValueError("n must be positive")

    rv = []
    line = ""
    for word in para.split(" "):
        if len(line) + 1 + len(word) >= n:
            rv.append(line)
            line = ""
        if line == "":
            line = "{}{}".format(m * " ", word)
        else:
            line = "{} {}".format(line, word)
    if line != "":
        rv.append(line)
    return "\n".join(rv)


@lru_cache()
def load_dataset(root: str, dataset: str, separator: str = "\n", data_type: DTT = str) -> List[DT]:
    """ Load an existing dataset from a file

    The existing datasets are grouped by `root`:

    * ``"general"`` - problem agnostic datasets (e.g. famous numeric sequences)
    * ``"problems"`` - problem specific datasets (i.e. provided by Project Euler)

    The `dataset` is a filename that is expected to be found at `data`/``root``/``dataset`` with a `.txt` extension. The
    `separator` will be used to split the dataset. Finally, the `data_type` parameter can be used to
    `typecast <https://en.wikipedia.org/wiki/Type_conversion>`_ each element in the dataset.

    :param root: the root, or type, of dataset to load from
    :param dataset: the specific dataset (without a `.txt` extension) to load
    :param separator: the string to split elements on
    :param data_type: the underlying data-type of the elements in this dataset
    :return: a list of elements from the specified dataset with typecasting
    :raises TypeError: if `root`, `dataset`, `separator` or `data_type` are not ``str`` variables
    :raises FileNotFoundError: if the requested file (`root`, `dataset`) doesn't exist
    :raises ValueError: if an element of the dataset cannot be typecast to `data_type`

    .. note:: if `separator` is ``""`` (i.e. the empty string), no splitting occurs.

    .. note:: ``DT`` is the type that was specified for `data_type`. That is, this function returns a list of elements,
              the type of which is set by `data_type`.

    .. warning:: there is a bug in the generation of this documentation. The default value of `separator` is the newline
                 character (i.e. ``"\\n"``), not the literal ``"n"`` as reported above.
    """

    # Parameter validation
    if not isinstance(root, str):
        raise TypeError("root must be a string")
    if not isinstance(dataset, str):
        raise TypeError("dataset must be a string")
    if not isinstance(separator, str):
        raise TypeError("separator must be a string")
    if not isinstance(data_type, type):
        raise TypeError("datatype must be a DTT")

    # Build the relative path to the requested dataset
    this_dir = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(this_dir, "..", DATA_DIR, root, "{}.txt".format(dataset))

    # Read the file contents
    with open(path, "r") as fp:
        contents = fp.read()

    contents = contents.rstrip("\n")  # strip off any trailing new-lines

    # If a separator is specified ...
    if separator != "":
        contents = contents.split(separator)  # ... split the datafile on the separator
        contents = list(map(data_type, contents))  # ... typecast each element to data_type

    return contents


def memoize(func: Callable, maxsize: int = 128, typed: bool = False) -> Callable:
    """ Convenience function to memoize an existing function

    This is a simple wrapper around Python's ``functools.lru_cache`` which provides
    `memoization <https://en.wikipedia.org/wiki/Memoization>`_ to arbitrary functions. The purpose of memoization is to
    cache the result of evaluating `func` on the provided arguments so that repeated calls do not re-evaluate the
    function unnecessarily. It is a useful building block in
    `dynamic programming algorithms <https://en.wikipedia.org/wiki/Dynamic_programming>`_.

    .. note:: it only makes sense to apply memoization to
              `deterministic functions <https://en.wikipedia.org/wiki/Deterministic_algorithm>`_.

    Python's ``functools.lru_cache`` employs the least recently used, or LRU, paradigm. There are two optional
    arguments.

    First, the `maxsize` argument sets the size of the cache. Powers of two are optimal values. A value of ``None``
    means there is no limit and all input values will result in a cache entry.

    .. warning:: having no restriction on the cache size can lead to memory leaks. Caution is advised.

    Second, the `typed` argument will enforce strict type checking. If `typed` is ``True``, then even if two objects can
    be coerced into equivalence, they will be considered as separate inputs, and thus, have separate cache records. For
    example, if `typed` is ``False`` (the default), then `func(3)` and `func(3.0)` would share a cache value, whereas is
    `typed` is ``True`` then they would have individual cache values.

    :param func: the function to memoize
    :param maxsize: the size of the LRU cache
    :param typed: whether to strictly enforce type-checking or not
    :return: a memoized function, functionally equivalent to `func`
    """

    @lru_cache(maxsize=maxsize, typed=typed)
    def helper(*args, **kwargs):
        """ Function stub that is decorated with Python's functools.lru_cache

        :param args: positional arguments
        :param kwargs: keyword arguments
        :return: the result of evaluating the memoized function on the provided arguments
        """

        return func(*args, **kwargs)

    return helper
