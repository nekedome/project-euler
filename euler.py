"""
:mod:`euler` -- Command Line Interface
======================================

.. module:: euler
   :synopsis: Project's command line interface (CLI).

.. moduleauthor:: Bill Maroney <bill@invalid.com>
"""

import argparse
import importlib
import os
import pytest
import urllib.request
from bs4 import BeautifulSoup
from time import time
from typing import Dict

from lib.util import wrap
from tests.validation_test import SOLUTION_MODULE_ROOT, SOLUTION_MODULE_PATH

TEMPLATE_PATH = "problem_template"  # template used to start new problem
RST_TEMPLATE_PATH = "rst_template"  # template used to document new problem
BASE_URL = "https://projecteuler.net/"  # URL of Project Euler web-site


def start_problem(selection):
    """ Create a template solution Python file for a selected Project Euler problem

    The Python file will be populated by the problem description and boilerplate needed by this project.
    """

    template = _load_templates()  # load templates for Python and rst files

    # Ask user for the problem number (if it wasn't specified on command line)
    if selection is None:
        selection = input("Select a problem: ")

    # Parse the specified problem number
    err_msg = "Error: you must enter a positive decimal number."
    try:
        problem_number = int(selection)
    except ValueError:
        print(err_msg)
        return
    if problem_number <= 0:
        print(err_msg)
        return

    # Fetch the problem URL
    url = "{}problem={}".format(BASE_URL, problem_number)
    with urllib.request.urlopen(url) as fp:
        soup = BeautifulSoup(fp, "html.parser")

        # Extract the problem title, capitalise it
        title = soup.find("h2").text
        title = " ".join([word if word.isupper() else word.title() for word in title.split(" ")])

        # Extract the problem statement
        problem = soup.find("div", attrs={"class": "problem_content"})
        paragraphs = problem.text.split("\n")
        problem_statement = ""
        for paragraph in [paragraph for paragraph in paragraphs if paragraph != ""]:
            problem_statement += wrap(paragraph, 0, 120)
            problem_statement += "\n\n"
        problem_statement = problem_statement.rstrip("\n")

        problem_statement = problem_statement.encode("ascii", "ignore").decode()

        # Write the problem templates (if they don't already exist)
        path = _build_template_paths(problem_number)
        if not os.path.exists(path["py"]):
            with open(path["py"], "w") as op:
                uline = "=" * len("Project Euler Problem {id}: {title}".format(id=problem_number, title=title))
                op.write(template["py"].format(id=problem_number, title=title, underline=uline, problem=problem_statement))
        else:
            print("Error: {} already exists.".format(path["py"]))
            return
        if not os.path.exists(path["rst"]):
            with open(path["rst"], "w") as op:
                op.write(template["rst"].format(id=problem_number))
        else:
            print("Error: {} already exists.".format(path["rst"]))
            return

        _fetch_downloads(problem)


def solve_problem(selection):
    """ Dynamically load and run solution for a selected Project Euler problem """

    # Ask user for the problem number (if it wasn't specified on command line)
    if selection is None:
        selection = input("Select a problem: ")

    # Parse the specified problem number
    err_msg = "Error: you must enter a positive decimal number."
    try:
        problem_number = int(selection)
    except ValueError:
        print(err_msg)
        return
    if problem_number <= 0:
        print(err_msg)
        return

    if isinstance(problem_number, int):
        # Attempt to dynamically load the solution module
        try:
            mod = importlib.import_module(SOLUTION_MODULE_PATH.format(problem_number))
        except ModuleNotFoundError:
            print("Error: solution for problem {} doesn't currently exist.".format(problem_number))
            return  # cannot proceed without the problem module

        # Attempt to compute the solution and check its correctness
        try:
            t0 = time()
            answer = mod.solve()
            t1 = time()
        except AttributeError as err:
            print("Error: {}.".format(err))
            return  # cannot proceed with problem modules solve function

        # Attempt to retrieve the expected answer
        try:
            expected_answer = mod.expected_answer
        except AttributeError as err:
            expected_answer = None
            print("Warning: {}.".format(err))

        # Report the results and runtime
        if expected_answer is None:
            suffix = "cannot be checked."
        elif expected_answer == answer:
            suffix = "is correct."
        else:
            suffix = "is incorrect.\nExpected solution is {}.".format(expected_answer)
        print("Solution is {}, which {}".format(answer, suffix))
        print("Solution took {:.2f} seconds to compute.".format(t1 - t0))


def _build_args_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="command")

    parser_start = subparsers.add_parser("start", help="Start a new Project Euler problem")
    parser_start.add_argument("n", nargs="?", type=int, help="problem number (default: prompt user for input)")

    parser_solve = subparsers.add_parser("solve", help="Compute the answer to a single Project Euler problem")
    parser_solve.add_argument("n", nargs="?", type=int, help="problem number (default: prompt user for input)")

    parser_validate = subparsers.add_parser("validate", help="Compute answers to all Project Euler problems")

    return parser


def _load_templates() -> Dict[str, str]:
    # Load the problem template to be instantiated with this new problem
    with open(os.path.join(os.path.dirname(__file__), TEMPLATE_PATH), "r") as fp:
        py_template = fp.read()

    # Load the reStructuredText template to be instantiated with this new problem
    with open(os.path.join(os.path.dirname(__file__), RST_TEMPLATE_PATH), "r") as fp:
        rst_template = fp.read()

    return {"py": py_template, "rst": rst_template}


def _build_template_paths(problem_number: int) -> Dict[str, str]:
    base_path = os.path.dirname(__file__)
    py_path = os.path.join(base_path, "solutions", "problem{}.py".format(problem_number))
    rst_path = os.path.join(base_path, "docs", "solutions", "{}.rst".format(problem_number))
    return {"py": py_path, "rst": rst_path}


def _fetch_downloads(problem: str) -> None:
    # Search for possible file attachments and prompt for optional downloads
    files = problem.find_all("a")
    for file in files:
        file_url = "{}{}".format(BASE_URL, file["href"])
        filename = file["href"].split("/")[-1]
        file_path = os.path.join("data", "problems", filename)

        if os.path.exists(file_path):
            print("Warning: {} already exists.".format(file_path))
            continue

        choice = input("Download '{}'? [y/N]: ".format(filename))
        if choice == 'y':
            with urllib.request.urlopen(file_url) as ffp:
                data = ffp.read()
                open(file_path, "w").write(data.decode("utf8"))


def main():
    """ Main program entry point """

    # Build an argument parser and apply it to the command-line arguments
    parser = _build_args_parser()
    args = parser.parse_args()

    # Perform the requested action
    if args.command == "start":
        # Start a problem
        start_problem(args.n)
    elif args.command == "solve":
        # Execute a single solution displaying answer (for submission to Project Euler)
        solve_problem(args.n)
    elif args.command == "validate":
        # Validate all existing solutions
        pytest.main(["tests/validation_test.py"])  # run the pytest unit-testing framework
    else:
        parser.print_usage()  # invalid command


if __name__ == "__main__":
    main()
